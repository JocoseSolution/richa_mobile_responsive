﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage_Test.master" AutoEventWireup="false"
    CodeFile="Login.aspx.vb" Inherits="Login" %>

<%@ Register Src="~/UserControl/LoginControl.ascx" TagPrefix="UC1" TagName="Login" %>
<%@ Register Src="~/UserControl/IssueTrack.ascx" TagName="Holiday" TagPrefix="ucholiday" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

<UC1:Login ID="uc" runat="server"/>
      
    <style type="text/css">
        @media only screen and (max-width:500px) {

            .top-logo, .affix-top {
                display: none !important;
            }
        }
    </style>  
<div class="content_section_left" style="display: none">
            <div class="product_box">
                <div class="totoPopup">
                    <a href="Adds/Offers/ViewOffers.aspx?imgname=homeb1" target="_blank" id="ancHomeBott1">
                        <img src="" id="imgHomeb1" />
                    </a>
                </div>
              
                <%--<div class="protxt">
                    <span id="spnHomeb1">Tashkent, the pearl of East</span>
                    <div id="divhomebdur1">
                        03 Nights 04 Days@36,499/- PP</div>
                </div>--%>
            </div>
            <%--<div class="product_box">
                <div>
                    <a target="_blank" href="Adds/Offers/ViewOffers.aspx?imgname=homeb2" id="ancHomeBott2">
                        <img src="" alt="" id="imgHomeb2" />
                    </a>
                </div>
                <div class="clear1">
                </div>
                <div class="protxt">
                    <span id="spnHomeb2">Enchanting Kerala</span>
                    <div id="divhomebdur2">
                        05 Nights 06 Days@15,499/-</div>
                </div>
            </div>--%>
            <div class="product_box">
                <%--<div>
                    <a target="_blank" href="Adds/Offers/ViewOffers.aspx?imgname=homeb3" id="ancHomeBott3">
                        <img src="" id="imgHomeb3" />
                    </a>
                </div>--%>
                 
                <%--<div class="protxt">
                    <span id="spnHomeb3">Singapore With Cruise</span>
                    <div id="divhomebdur3">
                        05 Nights 06 Days@63,999/-</div>
                </div>--%>
            </div>
            <div style="clear: both;">
            </div>
        </div>
         
  
    

    
    <div class="toPopup1" style="display: none;">
        <div style="width: 32%; padding: 1%; margin: 50px auto 0; background: #f9f9f9;">
            <div style="font-weight: bold;">
                Timings 10 AM to 6 PM, Monday to Saturday. Issues Reported after 6 PM and on Sunday will be addressed on the following day.
            </div>
            <div>
                <ucholiday:Holiday ID="uc_holiday" runat="server" />
            </div>
        </div>
    </div>
    <div id="boxssss" style="display:none;">
        <img id="image" src=""/>
    </div>

    
</asp:Content>
