﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage_Test.master" AutoEventWireup="false" CodeFile="about-us.aspx.vb" Inherits="about_us" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    
    <section>
		<div class="rows inner_banner inner_banner_2">
			<div class="container">
				<h2><span>About us -</span> Richa World Travels</h2>
			    <p>Book Flight Ticket and enjoy your holidays with distinctive experience</p>
			</div>
		</div>
	</section>

    <section class="tourb2-ab-p-2 com-colo-abou" >
		<div class="container">
			<!-- TITLE & DESCRIPTION -->
			<%--<div class="spe-title">
				<h2>Top <span>tour packages</span> in this month</h2>
				<div class="title-line">
					<div class="tl-1"></div>
					<div class="tl-2"></div>
					<div class="tl-3"></div>
				</div>
				<p>World's leading tour and travels Booking website,Over 30,000 packages worldwide. Book travel packages and enjoy your holidays with distinctive experience</p>
			</div>--%>
			<div class="row tourb2-ab-p1" style="margin-top: 0px;">
				<div class="col-md-12 col-sm-12">
					<div class="tourb2-ab-p1-left">
						<h3>Hi! Welcome to Richa World Travels</h3>
						<p>We are on mission to help agents discover the real value of booking - to inspire, to give more reasons, to make it easy for agents to more financial gain with accuracy. Our company was founded back in 1999, and since then, we've imagined and created some of the most well-loved products for travel agents all around the world.</p>
						<p>Today, b2brichatravels.in is used by thousands of agents every month - Agent who booked for their financial gain, convenience and for many other reasons. That's why we work tirelessly to make your experience of booking flights, hotel, Railway and bus as seamless as possible. Because of - Since the year 2006, the company is being awarded by some of the most esteemed airlines for its outstanding provision of services.</p> 
                        <p>Richa World Travels is more than just a website or company. Richa World Travels is for belief of agents that every agents has for their travelers, to distinctive experience of their travel and to grow.</p><a href="#" class="link-btn">Call Us : +91 79 4910 9999</a> </div>
				</div>
				<div class="col-md-6 col-sm-6">
					<div class="tourb2-ab-p1-right"> <img src="images/iplace-8.jpg" alt=""> </div>
				</div>
			</div>
		</div>
	</section>


    




    <section class="tourb2-ab-p-3 com-colo-abou">
      
		<div class="container">
             <div class="spe-title">
				<h2 ><span style="font-size: 24px;">Available Services</span> </h2>
				<div class="title-line">
					<div class="tl-1"></div>
					<div class="tl-2"></div>
					<div class="tl-3"></div>
				</div>
				
			</div>
			<div class="row tourb2-ab-p3">
				<div class="col-md-3 col-sm-6">
					<div class="tips_travel_2">
                      <i class="fa fa-plane" aria-hidden="true"></i>
							
						
						<h4 style="color:#e7e7e7">Flight Booking</h4>
						</div>
					
				</div>
				<div class="col-md-3 col-sm-6">
					<div class="tips_travel_2"> 
                        <i class="fa fa-train" aria-hidden="true"></i>
						<h4 style="color:#e7e7e7">Rail Booking</h4>
						
					</div>
				</div>
				<div class="col-md-3 col-sm-6">
					<div class="tips_travel_2">
                         <i class="fa fa-bus" aria-hidden="true"></i>
						<h4 style="color:#e7e7e7">Bus Booking</h4>
						
					</div>
				</div>
				<div class="col-md-3 col-sm-6">
					<div class="tips_travel_2">
                         <i class="fa fa-hotel" aria-hidden="true"></i>
						<h4 style="color:#e7e7e7">Hotel Booking</h4>
						
					</div>
				</div>
			</div>
		</div>
	</section>

    <section class="tourb2-ab-p-4 com-colo-abou" style="padding:0px;">
		<div class="container">

            <div class="spe-title">
				<h2 ><span style="font-size: 24px;">Why Book with Richa World Travels?</span> </h2>
				<div class="title-line">
					<div class="tl-1"></div>
					<div class="tl-2"></div>
					<div class="tl-3"></div>
				</div>
				
			</div>


			<div class="row tourb2-ab-p4">
                <div class="row">
				<div class="col-md-4 col-sm-4">
                    
			<div class="tips_left">
                <i><img src="Images/mail/search.png" style="width:35px"/></i>
						<h4 style="color: #09a9e5;">Look No Further</h4>
						<p>Search all the airline fares in one go</p>
					</div>


				</div>
				<div class="col-md-4 col-sm-4">
				<div class="tips_left">
                    <i><img src="Images/mail/wallet-512.png" style="width:35px"/>
                    </i>
						<h4 style="color: #09a9e5;">Book with Confideance</h4>
						<p>No hidden fees, taxes of other nasty surprise</p>
					</div>

				</div>
				<div class="col-md-4 col-sm-4">
				<div class="tips_left">
                    <i><img src="Images/mail/img_460875.png" style="width:35px"/></i>
						<h4 style="color: #09a9e5;">Pay net Only</h4>
						<p>Pay net fares for your all booking, profite will come direct on your hands.</p>
					</div>

				</div>
                </div>
                <div class="row">
				<div class="col-md-4 col-sm-4">
			
                     <div class="tips_left">
                         <i><img src="Images/mail/Aviation_excalibur-512.png" style="width:35px"/></i>
						<h4 style="color: #09a9e5;">Instant Booking</h4>
						<p>For all products, book with couples of tickets.</p>
					</div>

				</div>
				<div class="col-md-4 col-sm-4">
				
                     <div class="tips_left">
                         <i><img src="Images/mail/refund.png" style="width:35px"/></i>
						<h4 style="color: #09a9e5;">Easy Cancellation & Refunds</h4>
						<p>Get transparency refunds and accounts</p>
					</div>

				</div>
				<div class="col-md-4 col-sm-4">
			

                     <div class="tips_left">
                         <i><img src="Images/mail/Cheap_price.png" style="width:35px"/></i>
						<h4 style="color: #09a9e5;">Cheapest Price</h4>
						<p>Always get best market deals with no hidden charges or taxes.</p>
					</div>


				</div>
                </div>

			</div>
		</div>
	</section>

</asp:Content>

