﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PriceDetails.aspx.vb" MasterPageFile="~/MasterAfterLogin.master"   Inherits="FlightDom_PriceDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%-- <link href="../CSS/astyle.css" rel="stylesheet" />--%>
    <style type="type/css">
        .f18 {
            font-size: 18px;
        }
        .blds {
            color: #004b91 !important;font-weight:bold !important;
        }
          
    </style>

    <style type="text/css">
          .table {
    display: table;
    text-align: center;
    width: 90%;
    margin: 2% auto 20px;
    border-collapse: separate;
    font-family: 'Roboto', sans-serif;
    font-weight: 400;
  }
  
  .table_row {
    display: table-row;
  }
  
  .theader {
    display: table-row;
  }
  
  .table_header {
display: table-cell;
    border-bottom: #040404 1px solid;
    border-top: #000 1px solid;
    background: #ffffff;
    color: #909090;
    padding-top: 10px;
    padding-bottom: 10px;
    font-weight: 700;
    border-right: 1px solid #000;
  }
  
  .table_header:first-child {
   border-right: #000000 1px solid;
    border-top-left-radius: 5px;
    border-left: #000000 1px solid;
  }
  
  .table_header:last-child {
    border-right: #000 1px solid;
    border-top-right-radius: 5px;
  }
  
  .table_small {
    display: table-cell;
  }
  
  .table_row > .table_small > .table_cell:nth-child(odd) {
    display: none;
    background: #bdbdbd;
    color: #e5e5e5;
    padding-top: 10px;
    padding-bottom: 10px;
  }
  
  .table_row > .table_small > .table_cell {
    padding-top: 3px;
    padding-bottom: 3px;
    color: #5b5b5b;
    border-bottom: #000 1px solid;
    border-right: 1px solid #000;
  }
  
  .table_row > .table_small:first-child > .table_cell {
    border-left: #000 1px solid;
    border-right: #000 1px solid;
  }
  
  .table_row > .table_small:last-child > .table_cell {
    border-right: #000 1px solid;
  }
  
  .table_row:last-child > .table_small:last-child > .table_cell:last-child {
    border-bottom-right-radius: 5px;
  }
  
  .table_row:last-child > .table_small:first-child > .table_cell:last-child {
    border-bottom-left-radius: 0px;
  }
  
  .table_row:nth-child(2n+3) {
    background: #e9e9e9;
  }
  
  @media screen and (max-width: 900px) {
    .table {
      width: 90%
    }
  }
  
  @media screen and (max-width: 650px) {
    .table {
      display: block;
    }
    .table_row:nth-child(2n+3) {
      background: none;
    }
    .theader {
      display: none;
    }
    .table_row > .table_small > .table_cell:nth-child(odd) {
      display: table-cell;
      width: 50%;
    }
    .table_cell {
      display: table-cell;
      width: 50%;
    }
    .table_row {
      display: table;
      width: 100%;
      border-collapse: separate;
      padding-bottom: 20px;
      margin: 5% auto 0;
      text-align: center;
    }
    .table_small {
      display: table-row;
    }
    .table_row > .table_small:first-child > .table_cell:last-child {
      border-left: none;
    }
    .table_row > .table_small > .table_cell:first-child {
      border-left: #ccc 1px solid;
    }
    .table_row > .table_small:first-child > .table_cell:first-child {
      border-top-left-radius: 5px;
      border-top: #ccc 1px solid;
    }
    .table_row > .table_small:first-child > .table_cell:last-child {
      border-top-right-radius: 5px;
      border-top: #ccc 1px solid;
    }
    .table_row > .table_small:last-child > .table_cell:first-child {
      border-right: none;
    }
    .table_row > .table_small > .table_cell:last-child {
      border-right: #ccc 1px solid;
    }
    .table_row > .table_small:last-child > .table_cell:first-child {
      border-bottom-left-radius: 5px;
    }
    .table_row > .table_small:last-child > .table_cell:last-child {
      border-bottom-right-radius: 5px;
    }
  }
    </style>

  <link href="../Custom_Design/css/skunal.css" rel="stylesheet" />


    <link href="<%= ResolveUrl("~/Styles/jAlertCss.css")%>" rel="stylesheet" />
    <script language="javascript" type="text/javascript">
   
        $(document).ready(function () {
            var rslt = false;
            $("#ctl00_ContentPlaceHolder1_Submit").click(function (e) {
                //e.preventDefault();
               
              jConfirm('Are you sure!', 'Confirmation', function (r) {

                    if (r) {
                      
                        document.getElementById("div_Submit").style.display = "none";
                        document.getElementById("div_Progress").style.display = "block";
                        rslt = true;

                        $('#ctl00_ContentPlaceHolder1_Submit').unbind('click').click();
                    }
                    else {
                        rslt= false;
                    }

                });
              
              return rslt;
                //if (confirm("Are you sure!")) {

                //    return true;
                //}
                //else {
                //    return false;
                //}
            });
        });
    </script>




    
    <div style="text-align: right; width: 20%; float:right; padding:5px; display:none; ">
        <input type="button" id="btnBookAnother" value="Book Other Flight" style="width:200PX;" class="buttonfltbk" name="<%=Session("SearchCriteriaUser").ToString()%>" />
    </div>
    
    <div style="padding:11px;">
    <div class="large-10 medium-10 small-12 large-push-1 medium-push-1">
        <div class="large-12 medium-12 small-12">

         <div class="clear">
                </div>
                <div class="clear">
                </div>
        <%--<div class="f16 bgf1 bld padding1">
            Itinerary Details</div>
        <hr />--%>
        
        <div  id="divFltDtls" runat="server">
        </div>
        <%--<img src="../../Images/edit.png" alt="Edit" />--%>

        <asp:LinkButton ID="LinkEdit" Text="Edit" runat="server" Font-Bold="True" style="font-weight: bold;right: -954px;top: 45px;color: white;position:relative;z-index:1011;"></asp:LinkButton>
        <div class="large-12 medium-12 small-12 bor" id="divPaxdetails" runat="server" style="background-color:#fff; margin-top:10px; " >
            
        
        
        </div>
         <div class="large-12 medium-12 small-12  " id="SeatInformation" runat="server" style="background-color:#fff; margin-top:10px; " >
            
        </div>


        <div class='row' id="PaxGrd_div" runat="server" style='background-color:#fff;WIDTH: 98%;' >
        <div class='fd-h'><i class="icofont-travelling"></i> Traveller Information</div>
         <div class='clear1'></div>
            <div class='large-12 medium-12 small-12'></div>

            <div>
              <table>
                  <tr>
                 
                         <td>
                           <asp:Label runat="server"><b>Mobile:</b></asp:Label>
                          <asp:TextBox ID="Mob_txt" runat="server" MaxLength="10"></asp:TextBox>
                           <asp:RequiredFieldValidator ID="RequiredFieldValidator21" ControlToValidate="Mob_txt" runat="server"
                                   ErrorMessage="Mobile Number Is Required" ForeColor="Red"></asp:RequiredFieldValidator>

                                   <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="Mob_txt" ErrorMessage="Invalid Mobile"  ValidationExpression="[0-9]{10}"></asp:RegularExpressionValidator>  
                      </td>

     
                      <td>
                            <asp:Label runat="server"><b>Email:</b></asp:Label>
                           <asp:TextBox ID="Email_txt"  runat="server"></asp:TextBox>         
                             <asp:RequiredFieldValidator ID="RequiredFieldValidator20" ControlToValidate="Email_txt" runat="server"
                                   ErrorMessage="EmailID Is Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                          
                          <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ErrorMessage="Invalid Email" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="Email_txt"></asp:RegularExpressionValidator> 

                      </td>
                         
                  </tr>
              </table>
            </div>

          
            <asp:GridView ID="PAXGRD" runat="server" AutoGenerateColumns="false"  OnRowDataBound="gvDetails_RowDataBound">
                  <Columns>
                    
                     <asp:TemplateField HeaderText="Title"  ItemStyle-Width="100px">
                         <ItemTemplate>
                               <asp:Label ID="lbl_Tittle" Visible="false" runat="server" Text='<%#Eval("Title")%>'/>
                               <asp:DropDownList  ID="GGDD_DISTYPE" runat="server" Style="margin-top:-20px"  class="form-control">
                                                <asp:ListItem Text="Mr" Value="Mr"></asp:ListItem>                                              
                                                 <asp:ListItem Text="Miss" Value="Miss"></asp:ListItem>    
                                                 <asp:ListItem Value="Ms">Ms</asp:ListItem>
                                                 <asp:ListItem Value="Mrs">Mrs</asp:ListItem>       
                                                 <asp:ListItem Value="Mstr">Mstr</asp:ListItem>
                                                                                              
                       </asp:DropDownList>
                         </ItemTemplate>
                     </asp:TemplateField>

                       <asp:TemplateField>
                          <ItemTemplate>
                          <asp:TextBox ID="lbl_PaxID" Visible="false" runat="server" Text='<%#Eval("PaxId")%>'/>
                          </ItemTemplate>      
                          </asp:TemplateField>

                           <asp:TemplateField HeaderText="FName" ItemStyle-Width="250px">
                          <ItemTemplate>
                          <asp:TextBox ID="lbl_FName" runat="server" Text='<%#Eval("FName")%>'/>
                          <asp:RequiredFieldValidator ID="Requiredlbl_FName" ControlToValidate="lbl_FName" runat="server"
                          ErrorMessage="First Name Is Required" ForeColor="Red"></asp:RequiredFieldValidator>
                          </ItemTemplate>      
                          </asp:TemplateField>


                           <asp:TemplateField HeaderText="MName" ItemStyle-Width="250px">
                          <ItemTemplate>
                          <asp:TextBox ID="lbl_MName" Style="margin-top:-20px" runat="server" Text='<%#Eval("MName")%>'/>
                          </ItemTemplate>      
                          </asp:TemplateField>


                           <asp:TemplateField HeaderText="LName" ItemStyle-Width="250px">
                          <ItemTemplate>
                          <asp:TextBox ID="lbl_LName" runat="server" Text='<%#Eval("LName")%>'/>
                          <asp:RequiredFieldValidator ID="Requiredlbl_LName" ControlToValidate="lbl_LName" runat="server"
                          ErrorMessage="Last Name Is Required" ForeColor="Red"></asp:RequiredFieldValidator>
                          </ItemTemplate>      

                          </asp:TemplateField>

                          <asp:TemplateField HeaderText="Type" ItemStyle-Width="250px">
                          <ItemTemplate>
                          <asp:Label ID="lbl_PaxType"  runat="server" Text='<%#Eval("PaxType")%>'/>
                          </ItemTemplate>      
                          </asp:TemplateField>

                     <asp:TemplateField HeaderText="DOB" ItemStyle-Width="250px">
                    <ItemTemplate>
                     <asp:TextBox ID="Gtxt_DOB"  CssClass='<%#Eval("PaxType")%>' runat="server" Text='<%#Eval("DOB")%>'/>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="Gtxt_DOB" runat="server" ErrorMessage="Required DOB"></asp:RequiredFieldValidator>
                    </ItemTemplate>      
                    </asp:TemplateField>
             
                                                
            </Columns>

            </asp:GridView>


        </div>

        <div class="large-12 medium-12 small-12 bor" id="divFareDtls" runat="server"  style="background-color:#fff; margin-top:10px;"></div>
       

        <div class="" id="divFareDtlsR" runat="server">
        </div>
      
         <div class="large-12 medium-12 small-12 bor" style="background-color: #fff; margin-top: 10px;">
           <div class="large-12 medium-12 small-12  headbgs fd-h" style="line-height: 28px !important;"><i class="fa fa-inr" aria-hidden="true"></i> Payment Mode:</div>
             <asp:RadioButtonList ID="rblPaymentMode" runat="server" RepeatDirection="Horizontal" style="margin: 23px;">                                        
                 <asp:ListItem Text="Wallet" Selected="True" Value="WL"><img src="../Images/icons/x-70-512.png" title="Wallet" style="width:30px;"/></asp:ListItem>                                        
                 <%--<asp:ListItem Text="Payment Gateway" Value="PG"></asp:ListItem>
		     <asp:ListItem Text="Cash Card" Value="cashcard"></asp:ListItem>--%>
                 <asp:ListItem Text="Credit Card" Value="creditcard"><img src="../Images/icons/credit_card_PNG107.png" title="Credi Card" style="width:30px;"></asp:ListItem>                                        
                 <asp:ListItem Text="Debit Card" Value="debitcard"><img src="../Images/icons/debit.png" title="Debit Card" style="width:30px;"/></asp:ListItem>
                 <asp:ListItem Text="Net Banking"  Value="netbanking"><img src="../Images/icons/net-banking-png.png" title="Net Banking" style="width:30px;"/></asp:ListItem>   
                 <asp:ListItem Text="Paytm" Value="Paytm"><img src="../Images/icons/paytm-512.png" style="width:30px"/></asp:ListItem>
		 <asp:ListItem Text="Amex" Value="AMEX"><img src="../Images/icons/amex.png" title="Amex" style="width:30px"/></asp:ListItem>
		 <asp:ListItem Text="Diners" Value="DINR"><img src="../Images/icons/diner.png" title="Diners" style="width:30px"/></asp:ListItem>
                 <asp:ListItem Text="UPI" Value="upi"><img src="../Images/icons/UPI.png" title="UPI" style="width:30px;"/></asp:ListItem>                           
                 <asp:ListItem Text="Amazon Pay" Value="AMZPAY"><img src="../Images/icons/amazon.png" title="Amazon Pay" style="width:30px;"/></asp:ListItem>
                 <asp:ListItem Text="PhonePe" Value="PHONEPE"><img src="../Images/icons/phone-pe.png" title="PhonePe" style="width:30px;"/></asp:ListItem>                                   
            </asp:RadioButtonList>
        </div>
         <div id="div_Progress" style="display: none">
                <b>Booking In Progress.</b> Please do not 'refresh' or 'back' button
                <img alt="Processing.." src="<%= ResolveUrl("~/images/loading_bar.gif")%>" />
            </div>
       
</div>
     
        <asp:HiddenField ID="HdnTripType" runat="server" />       


        </div>
            <div class="clear">
        </div>
            <div class="clear">
        </div>


        <div class="container">
        <div >
             <asp:Label ID="lblHoldBookingCharge" runat="server" Text="" style="font-weight:800;font-size:14px;color:orange;"></asp:Label>
               </div>

        <div class="row">
     <div  id="div_Submit" class="col-md-12">

          <div class="col-md-2" style="float:right;">
                <asp:Button ID="Submit" class="btn btn-danger" runat="server" Text="Book" style="width: 104px;border-radius: 4px;"/>
                </div>


             <div class="col-md-1" style="float:right;">
                <asp:Button ID="ButtonHold" class="btn btn-danger" runat="server" Text="Hold" Visible="false" style="border-radius:4px;"/>
                </div>
            
            
           



        
        </div>


    
    </div>

        </div>
    </div>
         <br />
    <br />

         <div class="clear">
        </div>
       
        <div class="clear">
        </div>

    
   
  


     <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>    
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/alert.js")%>"></script>
    <script type="text/javascript">
        function funcnetfare(arg, id) {
            document.getElementById(id).style.display = arg

        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#btnBookAnother").click(function () {
                window.location.href = $.trim($(this).attr("name"))
            });
        });
$("#ctl00_ContentPlaceHolder1_rblPaymentMode").click(function () {
            GetPgTransCharge();          
        });
        function GetPgTransCharge() {         
            var checked_radio = $("[id*=ctl00_ContentPlaceHolder1_rblPaymentMode] input:checked");
            var PaymentMode = checked_radio.val();
            var tripType = $("#ctl00_ContentPlaceHolder1_HdnTripType").val();
            var NetFareOutBound = 0;
            var NetFareInBound = 0;
            var TotalNetFareOutBound = 0;
            var TotalNetFareInBound = 0;


            var FareOutBound = 0;
            var FareInBound = 0;
            var TotalFareOutBound = 0;
            var TotalFareInBound = 0;
            var PgChargOb = 0;
            var PgChargIb = 0;

            var TotalPgChargOb = 0.00;
            var TotalPgChargIb = 0.00;
            // 
            if (tripType == "InBound") {
                NetFareOutBound = $("#hdnNetFareOutBound").val();
                NetFareInBound = $("#hdnNetFareInBound").val();

                FareOutBound = $("#hdnTotalFareOutBound").val();
                FareInBound = $("#hdnTotalFareInBound").val();

            }
            else {
                NetFareOutBound = $("#hdnNetFareOutBound").val();
                FareOutBound = $("#hdnTotalFareOutBound").val();
            }
            
            if (PaymentMode != "WL") {
                // var PaymentMode = $("#ctl00_ContentPlaceHolder1_rblPaymentMode").val();            
                $.ajax({
                    type: "POST",
                    url: "PriceDetails.aspx/GetPgChargeByMode",
                    data: '{paymode: "' + PaymentMode + '" }',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        if (data.d != "") {
                            // 
                            if (data.d.indexOf("~") > 0) {
                                //var res = data.d.split('~');
                                var pgCharge = data.d.split('~')[0]
                                var chargeType = data.d.split('~')[1]

                                if (tripType == "InBound") {
                                    if (chargeType == "F") {                                       
                                        //calculate pg charge Fixed  of InBound
                                        PgChargOb = (parseFloat(pgCharge) / 2).toFixed(2);
                                        TotalNetFareOutBound = (parseFloat(NetFareOutBound) + parseFloat(PgChargOb)).toFixed(2);
                                        TotalFareOutBound = (parseFloat(FareOutBound) + parseFloat(PgChargOb)).toFixed(2);
                                                                          
                                        PgChargIb = (parseFloat(pgCharge) / 2).toFixed(2);
                                        TotalNetFareInBound = (parseFloat(NetFareInBound) + parseFloat(PgChargIb)).toFixed(2); 
                                        TotalFareInBound = (parseFloat(FareInBound) + parseFloat(PgChargIb)).toFixed(2);
                                        
                                        
                                        $('#PgChargeOutBound').html(PgChargOb);
                                        $('#lblNetFareOutBound').html(TotalNetFareOutBound);
                                        $('#divTotalFareOutBound').html(TotalFareOutBound);
                                       
                                        $('#PgChargeInBound').html(PgChargIb);
                                        $('#lblNetFareInBound').html(TotalNetFareInBound);
                                        $('#divTotalFareInBound').html(TotalFareInBound);                                    
                                        
                                    }
                                    else {
                                        //calculate pg charge Percentage of InBound
                                        PgChargOb = ((parseFloat(NetFareOutBound) * parseFloat(pgCharge)) / 100).toFixed(2);
                                        TotalNetFareOutBound = (parseFloat(NetFareOutBound) + parseFloat(PgChargOb)).toFixed(2);
                                        TotalFareOutBound = (parseFloat(FareOutBound) + parseFloat(PgChargOb)).toFixed(2);

                                        PgChargIb = ((parseFloat(NetFareInBound) * parseFloat(pgCharge)) / 100).toFixed(2);
                                        TotalNetFareInBound = (parseFloat(NetFareInBound) + parseFloat(PgChargIb)).toFixed(2);
                                        TotalFareInBound = (parseFloat(FareInBound) + parseFloat(PgChargIb)).toFixed(2);

                                        $('#PgChargeOutBound').html(PgChargOb);
                                        $('#lblNetFareOutBound').html(TotalNetFareOutBound);
                                        $('#divTotalFareOutBound').html(TotalFareOutBound);

                                        $('#PgChargeInBound').html(PgChargIb);
                                        $('#lblNetFareInBound').html(TotalNetFareInBound);
                                        $('#divTotalFareInBound').html(TotalFareInBound);
                                        
                                    }
                                }
                                else {
                                    //use for Outbound
                                    if (chargeType == "F") {
                                        //calculate pg charge Fixed of OutBound

                                        PgChargOb = (parseFloat(pgCharge)).toFixed(2);
                                        TotalNetFareOutBound = (parseFloat(NetFareOutBound) + parseFloat(PgChargOb)).toFixed(2);
                                        TotalFareOutBound = (parseFloat(FareOutBound) + parseFloat(PgChargOb)).toFixed(2);

                                        $('#PgChargeOutBound').html(PgChargOb);
                                        $('#lblNetFareOutBound').html(TotalNetFareOutBound);
                                        $('#divTotalFareOutBound').html(TotalFareOutBound);                                   

                                    }
                                    else {

                                        //calculate pg charge Percentage of OutBound

                                        PgChargOb = ((parseFloat(NetFareOutBound) * parseFloat(pgCharge)) / 100).toFixed(2);
                                        TotalNetFareOutBound = (parseFloat(NetFareOutBound) + parseFloat(PgChargOb)).toFixed(2);
                                        TotalFareOutBound = (parseFloat(FareOutBound) + parseFloat(PgChargOb)).toFixed(2);

                                        $('#PgChargeOutBound').html(PgChargOb);
                                        $('#lblNetFareOutBound').html(TotalNetFareOutBound);
                                        $('#divTotalFareOutBound').html(TotalFareOutBound);                                        
                                    }

                                }
                            }
                        }
                        else {
                            alert("try again");
                        }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert(textStatus);
                    }
                });

            }
            else {
                if (tripType == "InBound") {
                    // 
                    //calculate pg charge Fixed  of InBound
                    PgChargOb = "0.00";
                    TotalNetFareOutBound = (parseFloat(NetFareOutBound)).toFixed(2);
                    TotalFareOutBound = parseFloat(FareOutBound);

                    PgChargIb = "0.00";
                    TotalNetFareInBound = (parseFloat(NetFareInBound)).toFixed(2);
                    TotalFareInBound = parseFloat(FareInBound);
                    
                    $('#lblNetFareOutBound').html(TotalNetFareOutBound);
                    $('#divTotalFareOutBound').html(TotalFareOutBound);
                    $('#PgChargeOutBound').html(PgChargOb);

                    $('#lblNetFareInBound').html(TotalNetFareInBound);
                    $('#divTotalFareInBound').html(TotalFareInBound);
                    $('#PgChargeInBound').html(PgChargIb);

                }
                else {
                    //calculate pg charge Percentage of OutBound
                    PgChargOb = "0.00";
                    TotalNetFareOutBound = (parseFloat(NetFareOutBound)).toFixed(2);
                    TotalFareOutBound = parseFloat(FareOutBound);                

                    $('#lblNetFareOutBound').html(TotalNetFareOutBound);
                    $('#divTotalFareOutBound').html(TotalFareOutBound);
                    $('#PgChargeOutBound').html(PgChargOb);                   
                }

            }

        } 
    </script>
  
       <script type="text/javascript">
           var d = new Date();

           $(function () { var d = new Date(); var dd = new Date(1952, 01, 01); $(".ADT").datepicker({ numberOfMonths: 1, dateFormat: 'dd/mm/yy', changeMonth: true, changeYear: true, yearRange: ('1920:' + (d.getFullYear() - 12) + ''), navigationAsDateFormat: true, showOtherMonths: true, selectOtherMonths: true, defaultDate: dd }) });
           $(function () { $(".CHD").datepicker({ numberOfMonths: 1, dateFormat: 'dd/mm/yy', changeMonth: true, changeYear: true, maxDate: '-2y', minDate: '-12y', navigationAsDateFormat: true, showOtherMonths: true, selectOtherMonths: true }) });
           $(function () { $(".INF").datepicker({ numberOfMonths: 1, dateFormat: 'dd/mm/yy', changeMonth: true, changeYear: true, maxDate: '+0y', minDate: '-2y', navigationAsDateFormat: true, showOtherMonths: true, selectOtherMonths: true }) });
    </script>


</asp:Content>
