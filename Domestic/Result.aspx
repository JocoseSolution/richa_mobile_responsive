﻿<%@ Page Title="" Language="VB" AutoEventWireup="false" CodeFile="Result.aspx.vb"
    Inherits="FlightDom_FltResult" MasterPageFile="~/MasterAfterLogin.master" ValidateRequest="false" %>

<%--<%@ Register Src="~/UserControl/FltSearch.ascx" TagPrefix="uc1" TagName="FltSearch" %>--%>
<%@ Register Src="~/UserControl/FltSearchmdf.ascx" TagPrefix="uc1" TagName="FltSearch" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link href="../Styles/flightsearch.css" rel="stylesheet" type="text/css" />
    <%--<link href="../Styles/main.css" rel="stylesheet" />--%>
    <link href="../Styles/jquery-ui-1.8.8.custom.css" rel="stylesheet" type="text/css" />
    <link href="<%= ResolveUrl("css/itz.css") %>" rel="stylesheet" type="text/css" />

    <link href="../Custom_Design/css/my_design.css" rel="stylesheet" />
    <%--    <link href="CSS/newcss/tooltip.css" rel="stylesheet" type="text/css"/>--%>
    <link href="../Custom_Design/css/result.css" rel="stylesheet" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>


    <script type="text/javascript">
        $(document).ready(function (e) {
            $(".img-check").click(function () {
                $(this).toggleClass("check");
            });
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#flip").click(function () {
                $("#panel").slideToggle();



            });
        });
    </script>



    <script type="text/javascript">
        //$(document).ready(function () {
        //    $("#flip2").click(function () {
        //        $("#panel1").slideToggle();
        //        displayFDt();


        //    });
        //});


        function newwin(id) {

            debugger;
            $("#panel_" + id + "").slideToggle();
        }
    </script>


    <script type="text/javascript">
        $(document).ready(function () {
            $('#show-mod').click(function () {
                $('.modifySearch').slideToggle("slow");
                // Alternative animation for example
                // slideToggle("fast");
            });
        });


        $(document).ready(function () {
            $('#show-filter').click(function () {
                $('.passengersss').slideToggle("slow");
                // Alternative animation for example
                // slideToggle("fast");
            });
        });
    </script>

    <div class="w100" id="toptop">
        <div id="MainSFR">
            <div id="MainSF">
                <%--<div id="lftdv" class="hide w100 clear" onclick="fltrclick1(this.id)">
                    <div class="collapse"><< Collapse</div>
                    <div class="collapse hide">>> Expand</div>
                </div>--%>

                <div class="w100">
                    <input type="checkbox" class="chkNonstop" style="display: none" name="chkNonstop" id="chkNonstop" value="Y" />
                    <%--<div id="Modsearch" style="display: none; background: url(../images/blur-images-14.jpg); height: 100%; width: 100%; position: absolute; z-index: 99; left: 0; top: 0;">
                        <div style="width: 85%;margin: 50px auto;">
                            <div id="mdclose" style="width: 30px; height: 30px; border-radius: 50%; border: 3px solid #ccc; position: absolute; right: 10px; top: 10px; text-align: Center; font-size: 15px; background-color: black; cursor: pointer; color: #fff;">
                                X
                            </div>
                            <uc1:FltSearch runat="server" ID="FltSearch" />
                        </div>
                    </div>--%>



                    <div class="" id="lftdv1">
                        <div id="fltrDiv">
                            <%--searchtext--%>
                            <div id="mymod" class="">
                                <div id="dismod" class="clear passenger-2 modside">
                                    <div class="row" style="background: #156ebd; padding: 10px;">
                                        <div class="col-xs-12">
                                            <a onclick="closeMod()">X</a>
                                        </div>
                                    </div>
                                    <br />
                                    <br />
                                    <div class="container">
                                        <uc1:FltSearch runat="server" ID="FltSearch" />
                                    </div>

                                    <div class="lft bld colormn hide">
                                        <div id="divSearchText1">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="clear"></div>
                            <div class="row">
                                <%--<div id="dsplm" class="large-2 medium-3 small-6 columns passenger" onclick="DiplayMsearch1('DivMsearch');">
                                    <img src="../images/modify.png" />&nbsp; <span class="t5 pointer">Modify Search</span>
                                </div>--%>
                            </div>
                            <%-- <div class="clear"></div>
                            <hr />--%>

                            <%--<div class="bld f16">
                                <img src="../images/filter.png" />
                                Filter Search
                            </div>--%>


                            <div id="FilterLoad">
                            </div>
                        </div>


                        <div class="large-12 medium-12 small-12" style="margin-top: -6px;">

                            <div id="mysidenavssss" class="sidenavssss">

                                <div id="FilterBox lft">
                                    <div class="jplist-panel">
                                        <div class="row passengersss  wht w210 lft asbc lftflt " id="passengersss" style="border: 1px solid #dad8d8; box-shadow: 0 2px 0 0px rgba(0,0,0,.08); -webkit-box-shadow: 0 2px 0 0px rgba(0,0,0,.08); -moz-box-shadow: 0 2px 0 0px rgba(0,0,0,.08); overflow-x: scroll;">
                                            <div class="clear">
                                            </div>
                                            <div id="dsplm" style="width: 100%; float: left; background: #ffffff; -webkit-box-shadow: 0 2px 4px 0 rgba(0,0,0,.1); box-shadow: 0 2px 4px 0 rgba(0,0,0,.1); padding: 7px 0;">

                                                <div class="source-de7">
                                                    <div class="source-main7">
                    
                                                        <div class="source-right7">
                                                            <div class="arr-wht" id="backtoList" onclick="closeNav()"></div>
                                                            <div class="fltTxt">Filter and Sorting</div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div style="line-height: 10px; display: inline-block; vertical-align: top; padding: 0; float: RIGHT; padding-right: 3%; padding-top: 7px;">
                                                    <a href="#" class="" data-control-type="reset" data-control-name="reset" data-control-action="reset" style="float: left; margin-right: 9px; color: orange; margin-top: -5px; margin-left: 7px;">
                                                        <img alt="" src="https://cdn.onlinewebfonts.com/svg/img_341620.png" style="width: 20px;" /></a>
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                            <div class="w95 auto OnewayReturn" style="padding-top: 10px;">


                                                <div class="large-12 medium-12 small-12 columns passenger">
                                                    <%--<button type="button" class="jplist-reset-btn cursorpointer bld" data-control-type="reset" data-control-name="reset" data-control-action="reset" style="border: none; background: none;">
                                     <img src="../images/reset.png" style="position: relative; top: 3px;" />  &nbsp; Reset All Filters
                                        
                                    </button>--%>
                                                </div>


                                                <div class="large-12 medium-12 small-12">
                                                    <div id="flterTab" style="display: none">
                                                        <div id="flterTabO" class="spn1">
                                                            Outbound
                                                        </div>
                                                        <div class="lft w2">&nbsp;</div>
                                                        <div id="flterTabR" class="spn">
                                                            Inbound
                                                        </div>
                                                    </div>

                                                </div>



                                                <div class="large-12 medium-12 small-12 columns" id="FltrPrice">


                                                    <div class="prc_flt closeopen" onclick="fltrclick(this.id)" id="FBP">
                                                        <div class="prc_ttl">Price</div>

                                                    </div>

                                                    <div id="FBP1" class="w100 lft " style="display: none;">
                                                        <div class="clear2"></div>
                                                        <div class="fo">
                                                            <div class="clsone">
                                                                <div class="jplist-range-slider" data-control-type="range-slider" data-control-name="range-slider"
                                                                    data-control-action="filter" data-path=".price" style="left: 12px; position: relative;">
                                                                    <div class="clear1"></div>
                                                                    <div class="ui-slider w90 mauto" data-type="ui-slider">
                                                                    </div>
                                                                    <div class="clear1"></div>
                                                                    <div class="lft w45">
                                                                        <span class="lft">₹&nbsp;</span>
                                                                        <span class="value lft" data-type="prev-value"></span>
                                                                    </div>
                                                                    <div class="rgt w45" style="margin-right: 12px;">
                                                                        <span class="value rgt" data-type="next-value"></span>
                                                                        <span class="rgt">₹&nbsp;</span>
                                                                    </div>
                                                                </div>
                                                                <div class="hidden" data-control-type="default-sort" data-control-name="sort" data-control-action="sort"
                                                                    data-path=".price" data-order="asc" data-type="number">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="fr">
                                                            <div class="jplist-range-slider" data-control-type="range-sliderR" data-control-name="range-slider"
                                                                data-control-action="filter" data-path=".price">
                                                                <div class="clear1"></div>
                                                                <div class="ui-slider w90 mauto" data-type="ui-slider">
                                                                </div>
                                                                <div class="clear1"></div>
                                                                <div class="lft w45">
                                                                    <span class="lft">
                                                                        <img src="../images/rs.png" style="height: 13px;" />&nbsp;</span>
                                                                    <span class="value lft" data-type="prev-value"></span>
                                                                </div>
                                                                <div class="rgt w45">
                                                                    <span class="value rgt" data-type="next-value"></span>
                                                                    <span class="rgt">
                                                                        <img src="../images/rs.png" class="rgt" style="height: 13px;" />&nbsp;</span>
                                                                </div>
                                                            </div>
                                                            <div class="hidden" data-control-type="default-sort" data-control-name="sort" data-control-action="sort"
                                                                data-path=".price" data-order="desc" data-type="number">
                                                            </div>
                                                        </div>

                                                        <div class="clear">
                                                        </div>
                                                    </div>

                                                </div>
                                                <hr />
                                                <div class="col-md-12 col-sm-12 col-xs-12 columns" id="fltrTime">

                                                    <div class="bld closeopen closeopen1" onclick="fltrclick(this.id)" id="FBDT" style="color: #005999 !important;">
                                                        <div class="prc_flt">
                                                            <div class="prc_ttl">Departure Time</div>
                                                            <i class="fil_darw"></i><a class="resetfl" onclick="ResetTimming()" style="display: none">Reset</a>
                                                        </div>

                                                    </div>
                                                    <div id="FBDT1" class="w100 lft" style="overflow: hidden;">

                                                        <div class="prc_val ft1-sec takeoffTime fltTmg jplist-group" data-control-type="DTimefilterO" data-control-action="filter" data-control-name="DTimefilterO" data-path=".dtime" data-logic="or" style="display: block;">
                                                            <div class="tm-dt1">
                                                                <div class="tm11">
                                                                    <div class="tm-m11 ftRst">
                                                                        <div class="mor-n1 ftRstM">
                                                                            <input value="0_6" id="CheckboxT1" type="checkbox" title="Early Morning" />
                                                                        </div>
                                                                        <div class="clr"></div>
                                                                        <div class="clr"></div>
                                                                        <div class="fil-sbtxt">
                                                                            Before
                                                                            <br>
                                                                            6 AM
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="tm22">
                                                                    <div class="tm-m12 ftRst">

                                                                        <div class="mor1-n2 ftRstM">
                                                                            <input value="6_12" id="CheckboxT2" type="checkbox" title="Morning" />
                                                                        </div>
                                                                        <div class="clr"></div>
                                                                        <div class="clr"></div>
                                                                        <div class="fil-sbtxt">
                                                                            6 AM -<br>
                                                                            12 PM
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="tm33">
                                                                    <div class="tm-m22 ftRst">
                                                                        <div class="mor2-n3 ftRstM">
                                                                            <input value="12_18" id="CheckboxT3" type="checkbox" title="Mid Day" />
                                                                        </div>
                                                                        <div class="clr"></div>
                                                                        <div class="clr"></div>
                                                                        <div class="fil-sbtxt">
                                                                            12 PM -<br>
                                                                            6 PM
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="tm11-n">
                                                                    <div class="tm-m33 ftRst">
                                                                        <div class="mor3-n4 ftRstM">
                                                                            <input value="18_0" id="CheckboxT4" type="checkbox" title="Evening" />
                                                                        </div>
                                                                        <div class="clr"></div>
                                                                        <div class="clr"></div>
                                                                        <div class="fil-sbtxt">
                                                                            After
                                                                            <br>
                                                                            6 PM
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>



                                                        <div class="fo" style="display: none;">
                                                            <div class="jplist-group" style="position: relative;">
                                                                <div class="clear"></div>
                                                                <div class="row">
                                                                    <div class="lft wss20 abc1 bdrs">

                                                                        <label for="CheckboxT1"></label>
                                                                        <span>00 - 06</span>
                                                                    </div>
                                                                    <div class="lft wss20 abc2t bdrs">

                                                                        <label for="CheckboxT2"></label>
                                                                        <span>06 - 12</span>
                                                                    </div>

                                                                    <div class="lft wss20 abc3t bdrs">

                                                                        <label for="CheckboxT3"></label>
                                                                        <span>12 - 18</span>
                                                                    </div>
                                                                    <div class="lft wss20 abc4t bdrs">

                                                                        <label for="CheckboxT4"></label>
                                                                        <span>18 - 00</span>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="fr" style="display: none;">
                                                            <div class="jplist-group" data-control-type="DTimefilterR" data-control-action="filter" data-control-name="DTimefilterR" data-path=".atime" data-logic="or">

                                                                <div class="lft wss20 abc1 bdrs">
                                                                    <input value="0_6" id="CheckboxT1R" type="checkbox" title="Early Morning">
                                                                    <label for="CheckboxT1"></label>
                                                                    <span>00 - 06</span>
                                                                </div>
                                                                <div class="lft wss20 abc2t bdrs">
                                                                    <input value="6_12" id="CheckboxT2R" type="checkbox" title="Morning">
                                                                    <label for="CheckboxT2"></label>
                                                                    <span>06 - 12</span>
                                                                </div>

                                                                <div class="lft wss20 abc3t bdrs">
                                                                    <input value="12_18" id="CheckboxT3R" type="checkbox" title="Mid Day">
                                                                    <label for="CheckboxT3"></label>
                                                                    <span>12 - 18</span>
                                                                </div>
                                                                <div class="lft wss20 abc4t bdrs">
                                                                    <input value="18_0" id="CheckboxT4R" type="checkbox" title="Evening">
                                                                    <label for="CheckboxT4"></label>
                                                                    <span>18 - 00</span>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="clear">
                                                        </div>
                                                    </div>
                                                    <div class="clear">
                                                    </div>
                                                </div>
                                                <%--<div class="large-12 medium-12 small-12 columns" id="fltrTime" >
                                                
                                                <div class="bld closeopen" onclick="fltrclick(this.id)" id="FBDT"><i class="fa fa-clock-o" aria-hidden="true"></i>  Departure Time</div>
                                                <div id="FBDT1" class="w100 lft">
                                                    <div class="fo">
                                                        <div class="jplist-group"
                                                            data-control-type="DTimefilterO"
                                                            data-control-action="filter"
                                                            data-control-name="DTimefilterO"
                                                            data-path=".dtime" data-logic="or">
                                                            <div class="clear"></div>

                                                            <div class="lft wss20 abc1 bdrs bdrss" >
                                                                <input value="0_6" id="CheckboxT1" type="checkbox" title="Early Morning" />
                                                                <label for="CheckboxT1"></label>
                                                                <span>Early Morning</span><span style="float:right;">00 - 06</span>
                                                            </div>
                                                            <div class="lft wss20 abc2t bdrs">

                                                                <input value="6_12" id="CheckboxT2" type="checkbox" title="Morning" />
                                                                <label for="CheckboxT2"></label>
                                                                <span>Morning</span><span style="float:right;">06 - 12</span>
                                                            </div>

                                                            <div class="lft wss20 abc3t bdrs">
                                                                <input value="12_18" id="CheckboxT3" type="checkbox" title="Mid Day" />
                                                                <label for="CheckboxT3"></label>
                                                                <span>Mid-Day</span><span style="float:right;">12 - 18</span>
                                                            </div>
                                                            <div class="lft wss20 abc4t bdrs" >
                                                                <input value="18_0" id="CheckboxT4" type="checkbox" title="Evening" />
                                                                <label for="CheckboxT4"></label>
                                                                <span>Evening</span><span style="float:right;">18 - 00</span>
                                                            </div>

                                                            	
</div>


                                                        </div>
                                                    </div>
                                                    <div class="fr">
                                                        <div class="jplist-group"
                                                            data-control-type="DTimefilterR"
                                                            data-control-action="filter"
                                                            data-control-name="DTimefilterR"
                                                            data-path=".atime" data-logic="or">

                                                            <div class="lft wss20 abc1 bdrs">
                                                                <input value="0_6" id="CheckboxT1R" type="checkbox" title="Early Morning" />
                                                                <label for="CheckboxT1"></label>
                                                                <span>00 - 06</span>
                                                            </div>
                                                            <div class="lft wss20 abc2t bdrs">
                                                                <input value="6_12" id="CheckboxT2R" type="checkbox" title="Morning" />
                                                                <label for="CheckboxT2"></label>
                                                                <span>06 - 12</span>
                                                            </div>

                                                            <div class="lft wss20 abc3t bdrs">
                                                                <input value="12_18" id="CheckboxT3R" type="checkbox" title="Mid Day" />
                                                                <label for="CheckboxT3"></label>
                                                                <span>12 - 18</span>
                                                            </div>
                                                            <div class="lft wss20 abc4t bdrs">
                                                                <input value="18_0" id="CheckboxT4R" type="checkbox" title="Evening" />
                                                                <label for="CheckboxT4"></label>
                                                                <span>18 - 00</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="clear">
                                                    </div>
                                                </div>--%>
                                                

                                                <hr />

                                                <div class="col-md-12 col-sm-12 col-xs-12 columns">


                                                    <div class="prc_flt closeopen closeopen1" onclick="fltrclick(this.id)" id="FBA">
                                                        <div class="prc_ttl">Airlines</div>

                                                    </div>


                                                    <div class="prc_val" id="FBA1">
                                                        <div class="clear"></div>
                                                        <div id="airlineFilter" class="fo" style="overflow-y: auto; max-height: 270px; overflow-x: hidden; white-space: nowrap; left: 12px; position: relative;">
                                                            <div class="jplist-group" data-control-type="AirlinefilterO" data-control-action="filter" data-control-name="AirlinefilterO" data-path=".airlineImage" data-logic="or">
                                                                <div class="lft w8">
                                                                    <input value="Air Asia" id="CheckboxAO01" type="checkbox">
                                                                </div>
                                                                <div class="lft w80" style="padding-top: 3px;">
                                                                    <label for="Air Asia">Air Asia</label>
                                                                </div>
                                                                <div class="clear"></div>
                                                                <div class="lft w8">
                                                                    <input value="Air India" id="CheckboxAO11" type="checkbox">
                                                                </div>
                                                                <div class="lft w80" style="padding-top: 3px;">
                                                                    <label for="Air India">Air India</label>
                                                                </div>
                                                                <div class="clear"></div>
                                                                <div class="lft w8">
                                                                    <input value="Goair" id="CheckboxAO21" type="checkbox">
                                                                </div>
                                                                <div class="lft w80" style="padding-top: 3px;">
                                                                    <label for="Goair">Goair</label>
                                                                </div>
                                                                <div class="clear"></div>
                                                                <div class="lft w8">
                                                                    <input value="Indigo" id="CheckboxAO31" type="checkbox">
                                                                </div>
                                                                <div class="lft w80" style="padding-top: 3px;">
                                                                    <label for="Indigo">Indigo</label>
                                                                </div>
                                                                <div class="clear"></div>
                                                                <div class="lft w8">
                                                                    <input value="SpiceJet" id="CheckboxAO41" type="checkbox">
                                                                </div>
                                                                <div class="lft w80" style="padding-top: 3px;">
                                                                    <label for="SpiceJet">SpiceJet</label>
                                                                </div>
                                                                <div class="clear"></div>
                                                                <div class="lft w8">
                                                                    <input value="Vistara" id="CheckboxAO51" type="checkbox">
                                                                </div>
                                                                <div class="lft w80" style="padding-top: 3px;">
                                                                    <label for="Vistara">Vistara</label>
                                                                </div>
                                                                <div class="clear"></div>
                                                            </div>
                                                        </div>
                                                        <div id="airlineFilterR" class="fr" style="overflow: hidden auto; max-height: 270px; white-space: nowrap; display: none; left: 12px; position: relative;"></div>
                                                        <div class="clear"></div>
                                                    </div>

                                                </div>





                                                <%--                                            <div class="large-12 medium-12 small-12 columns">
                                                <div class="bld closeopen" onclick="fltrclick(this.id)" id="FBA"><i class="fa fa-plane" aria-hidden="true"></i>  Airline</div>
                                                <div class="w100 lft " id="FBA1">
                                                    <div class="clear"></div>
                                                    <div id="airlineFilter" class="fo" style="overflow-y: auto; max-height: 270px; overflow-x: hidden; white-space: nowrap;"></div>
                                                    <div id="airlineFilterR" class="fr" style="overflow-y: auto; max-height: 270px; overflow-x: hidden; white-space: nowrap;"></div>
                                                    <div class="clear"></div>
                                                </div>
                                                 
                                            </div>--%>
                                                <hr />


                                                <div class="large-12 medium-12 small-12 columns">


                                                    <div class="prc_flt closeopen" oonclick="fltrclick(this.id)" id="FBS">
                                                        <div class="prc_ttl">Stops</div>

                                                    </div>

                                                    <div class="w100 lft" id="FBS1">
                                                        <div class="clear"></div>
                                                        <div id="stopFlter" class="fo" style="overflow-y: auto; max-height: 170px; overflow-x: hidden; white-space: nowrap; left: 12px; position: relative;"></div>
                                                        <div id="stopFlterR" class="fr" style="overflow-y: auto; max-height: 170px; overflow-x: hidden; white-space: nowrap; left: 12px; position: relative;"></div>
                                                    </div>
                                                </div>

                                                <hr />

                                                <div class="large-12 medium-12 small-12 columns">


                                                    <div class="prc_flt closeopen" onclick="fltrclick(this.id)" id="FBFT">
                                                        <div class="prc_ttl">Fare Rules</div>

                                                    </div>

                                                    <div class="w100 lft" id="FBFT1" style="display: none;">
                                                        <div class="fo">
                                                            <div class="jplist-group"
                                                                data-control-type="RfndfilterO"
                                                                data-control-action="filter"
                                                                data-control-name="RfndfilteO"
                                                                data-path=".rfnd" data-logic="or" style="left: 12px; position: relative;">
                                                                <div class="clear"></div>
                                                                <div class="lft w8">
                                                                    <input value="r" id="CheckboxR1" type="checkbox" />
                                                                </div>
                                                                <div class="lft w80" style="padding-top: 3px">
                                                                    <label for="CheckboxR1">Refundable</label>
                                                                </div>
                                                                <div class="clear"></div>
                                                                <div class="lft w8">
                                                                    <input value="n" id="CheckboxR2" type="checkbox" />
                                                                </div>
                                                                <div class="lft w80" style="padding-top: 3px">
                                                                    <label for="CheckboxR2">Non Refundable</label>
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div class="fr">
                                                            <div class="jplist-group"
                                                                data-control-type="RfndfilterR"
                                                                data-control-action="filter"
                                                                data-control-name="RfndfilterR"
                                                                data-path=".rfnd" data-logic="or">
                                                                <div class="clear2"></div>
                                                                <div class="lft w8">
                                                                    <input value="r" id="Checkbox1" type="checkbox" />
                                                                </div>
                                                                <div class="lft w80" style="padding-top: 3px">
                                                                    <label for="CheckboxR1">Refundable</label>
                                                                </div>
                                                                <div class="clear"></div>
                                                                <div class="lft w8">
                                                                    <input value="n" id="Checkbox2" type="checkbox" />
                                                                </div>
                                                                <div class="lft w80" style="padding-top: 3px">
                                                                    <label for="CheckboxR2">Non Refundable</label>
                                                                </div>
                                                                <div class="clear"></div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                                <hr />
                                                <div class="large-12 medium-12 small-12" id="IdFareType">


                                                    <div class="prc_flt closeopen" onclick="fltrclick(this.id)" id="FBTY">
                                                        <div class="prc_ttl">Fare Types</div>

                                                    </div>

                                                    <div class="clsone lft w100" id="FBTY1">
                                                        <div class="fo">
                                                            <div class="jplist-group"
                                                                data-control-type="FareTypefilterO"
                                                                data-control-action="filter"
                                                                data-control-name="FareTypefilterO"
                                                                data-path=".srf" data-logic="or" style="left: 12px; position: relative;">
                                                                <div class="clear"></div>
                                                                <div class="lft w8">
                                                                    <input value="NRMLF" id="CheckboxFTY1" type="checkbox" />
                                                                </div>
                                                                <div class="lft w80" style="padding-top: 3px">
                                                                    <label for="CheckboxFTY1">Normal Fare</label>
                                                                </div>
                                                                <div class="clear"></div>
                                                                <div class="lft w8">
                                                                    <input value="SRF" id="CheckboxFTY2" type="checkbox" />
                                                                </div>
                                                                <div class="lft w80" style="padding-top: 3px">
                                                                    <label for="CheckboxFTY2">Special Return Fare</label>
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div class="fr">
                                                            <hr />
                                                            <br />
                                                            <div class="jplist-group"
                                                                data-control-type="FareTypefilterR"
                                                                data-control-action="filter"
                                                                data-control-name="FareTypefilterR"
                                                                data-path=".srf" data-logic="or">
                                                                <div class="clear2"></div>
                                                                <div class="lft w8">
                                                                    <input value="NRMLF" id="CheckboxFTYR1" type="checkbox" />
                                                                </div>
                                                                <div class="lft w80" style="padding-top: 3px">
                                                                    <label for="CheckboxFTYR1">Normal Fare</label>
                                                                </div>
                                                                <div class="clear"></div>
                                                                <div class="lft w8">
                                                                    <input value="SRF" id="CheckboxFTYR2" type="checkbox" />
                                                                </div>
                                                                <div class="lft w80" style="padding-top: 3px">
                                                                    <label for="CheckboxFTYR2">Special Return Fare</label>
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div class="clear"></div>
                                                    </div>
                                                    <div class="clear">
                                                    </div>

                                                </div>

                                          


                                                <div class="large-12 medium-12 small-12 columns">


                                                    <div class="prc_flt closeopen" onclick="fltrclick(this.id)" id="DAFT">
                                                        <div class="prc_ttl">Airline Fare Types</div>

                                                    </div>


                                                    <div class="w100 lft " id="DAFT1">
                                                        <div class="clear"></div>
                                                        <div id="AirlineFareType" class="FareTypeO fo" style="overflow-y: auto; max-height: 270px; overflow-x: hidden; white-space: nowrap; left: 9px; position: relative;"></div>
                                                        <div id="AirlineFareTypeR" class="FareTypeR fr" style="overflow-y: auto; max-height: 270px; overflow-x: hidden; white-space: nowrap; left: 9px; position: relative;"></div>
                                                        <div class="clear1">
                                                        </div>

                                                    </div>
                                                    <div class="clear">
                                                    </div>
                                                </div>


                                                <div class="apl7">
                                                    <input name="" type="button" class="apl27 applyFilter" onclick="closeNav()" value="Apply" />
                                                </div>
                                            </div>





                                        </div>


                                        <div class="w95 auto SpecialRTF" id="divFilterRTF" style="padding-top: 10px; display: none;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="fltbox2  w79 rgt jplist-panel" style="margin-top: -6px;">


                            <div class="clear"></div>

                            <div class="lft" id="refinetitle" style="display: none;"></div>

                            <div id="divMatrixRtfO" class="divMatrix w100"></div>



                            <div id="RoundTripH" class="flightbox">



                                <div class="row">
                                    <div class="laegr-12 medium-12 small-18">
                                        <div class="laegr-6 medium-6 small-6 columns">
                                            <div class="rd-trip">
                                                <div id="RTFTextFrom" class="lft destination1"></div>
                                                <a class="filt" href="#" style="float: right" onclick="openNav()"><i class="fa fa-filter fa-2x" aria-hidden="true" style="color: #fff;"></i></a>

                                                <div class="rgt hidden-xs" style="margin-right: 13px;">
                                                    <div class="auto lft">
                                                        <span onclick="ShowHideDiscount('show');" class="spnBtnShow" style="cursor: pointer;">Show&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                        <span onclick="ShowHideDiscount('hide');" style="display: none; cursor: pointer;" class="spnBtnHide">Hide&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                    </div>
                                                    <div class="auto lft">
                                                        <%--<input type="button" id="RtfFromPrevDay" value="Prev Day" class="pnday daybtn lft" /></div>--%>
                                                        <a href="#" id="RtfFromPrevDay" class="pnday lft" style="color: #fff">
                                                            <img src="../Styles/images/closeopen2.png" />&nbsp;Prev Day</a>
                                                    </div>
                                                    <div class="dotbdr lft">&nbsp;</div>
                                                    <div class="auto rgt">
                                                        <%--<input type="button" id="RtfFromNextDay" value="Next Day" class="pnday daybtn lft" /></div>--%>
                                                        <a href="#" id="RtfFromNextDay" class="pnday lft" style="color: #fff">Next Day&nbsp;<img src="../Styles/images/closeopen.png" />
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="laegr-6 medium-6 small-6 columns">
                                            <div class="rd-trip" style="">

                                                <div id="RTFTextTo" class="lft destination1"></div>
                                                <a class="mod" href="#" id="A1"><i class="fa fa-edit fa-2x" style="color: #fff;"></i></a>
                                                <div class="rgt hidden-xs" style="margin-right: 13px;">
                                                    <div class="auto lft">
                                                        <span onclick="ShowHideDiscount('show');" class="spnBtnShow" style="cursor: pointer;">Show&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                        <span onclick="ShowHideDiscount('hide');" style="display: none; cursor: pointer;" class="spnBtnHide">Hide&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                    </div>
                                                    <div class="auto lft">
                                                        <%--<input type="button" id="RtfToPrevDay" value="Prev Day" class="pnday daybtn lft" /></div>--%>
                                                        <a href="#" id="RtfToPrevDay" class="pnday lft" style="color: #fff">
                                                            <img src="../Styles/images/closeopen2.png" />&nbsp;Prev Day</a>
                                                    </div>
                                                    <div class="dotbdr lft">&nbsp;</div>

                                                    <div class="auto rgt">
                                                        <%--<input type="button" id="RtfToNextDay" value="Next Day" class="pnday daybtn lft" /></div>--%>
                                                        <a href="#" id="RtfToNextDay" class="pnday lft" style="color: #fff">Next Day&nbsp;<img src="../Styles/images/closeopen.png" />
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clear">
                                        </div>
                                    </div>
                                </div>
                                <div class="nav-container">

                                    <div class="nav">
                                        <div class="jplist-panel" style="position: fixed; bottom: 0; width: 100%; border: 1px solid #eee; right: 0; left: 0; z-index: 1011; background: #005999;">


                                            <div id="fltselct" style="display: none;">
                                                <%-- <div class="f16 w30 lft bld">Your Selection</div>--%>
                                                <div id="fltbtn" class="w70 rgt">




                                                    <div class="bld w33 lft difffare" id="FareDiff"></div>

                                                    <div class="lft w60 msg1" id="msg1">
                                                        PLEASE SELECT ONE<br />
                                                    </div>



                                                    <div id="Divproc" class="bld" style="display: none;">
                                                        <img alt="Booking In Progress" src="~/Images/loading_bar.gif" />
                                                    </div>
                                                    <div class="gridViewToolTip1 hide" id="fareBrkup" title="">ss</div>
                                                </div>

                                                <div class="detls">


                                                    <div id="selctcntnt" class="mauto">
                                                        <div class="col-md-3 col-xs-6">
                                                            <div id="fltgo" class="sec1" style="border-right: 1px solid #aeaeae; color: #000;">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-xs-6">
                                                            <div id="fltbk" class="sec1" style="color: #000;">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2" style="color: #000;">
                                                            <div class="" style="width: 337px;">
                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <div class="bld w33 lft prevfare" id="prevfare" style="font-size: 13px; color: #000;"></div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <div class="bld w28 lft currentfare" id="totalPay" style="font-size: 13px; color: #000;"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="gridViewToolTipSRF bld w5 lft" id="showfare">
                                                                <%--                                                                <img src='<%= ResolveClientUrl("~/images/icons/faredetails.png")%>' style="padding-top: 5px; padding-left: 10px; cursor: pointer;position:absolute;" alt="" />--%>
                                                                <a href="#" class="btn btn-default" style="color: #000;">Fare Breakup</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="col-md-3">
                                                                <input type="button" class="detls1 falsebookbutton" value="Book" id="FinalBook" style="Float: left; height: 32px; width: 150px;" />
                                                            </div>
                                                            <%-- <div class="col-md-2">
                                                            <input type="button" class="detls1" value="Show/Hide" style="Float: left;margin-left: 10px;" />
                                                                </div>--%>
                                                        </div>


                                                    </div>
                                                </div>
                                                <%-- <div class="clearfix"> </div>--%>
                                            </div>

                                        </div>
                                        <div class="row">
                                            <div class="laegr-12 medium-12 small-18">
                                                <!--headrow-->
                                                <div class="laegr-6 medium-6 small-6 columns">
                                                    <div class="one-side" style="">

                                                        <div class="col-md-12" style="position: relative; top: -4px;">
                                                            <div class="col-md-2 col-xs-4 colorwht srtarw abdd" onclick="myfunction(this)">
                                                                <div
                                                                    class=" "
                                                                    data-control-type="sortAirline"
                                                                    data-control-name="sortAirline"
                                                                    data-control-action="sort"
                                                                    data-path=".airlineImage"
                                                                    data-order="asc"
                                                                    data-type="text">
                                                                    Airline <i class="fa fa-caret-up"></i>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-8">
                                                                <div class="large-4 medium-4 small-4 columns colorwht abdd srtarw " onclick="myfunction(this)">
                                                                    <div
                                                                        class=" "
                                                                        data-control-type="sortDeptime"
                                                                        data-control-name="sortDeptime"
                                                                        data-control-action="sort"
                                                                        data-path=".deptime"
                                                                        data-order="asc"
                                                                        data-type="number">
                                                                        Depart <i class="fa fa-caret-up"></i>
                                                                    </div>

                                                                </div>

                                                                <div class="large-4 medium-4 small-4 columns colorwht abdd srtarw r-dep" onclick="myfunction(this)">
                                                                    <div
                                                                        class=" "
                                                                        data-control-type="sortTotdur"
                                                                        data-control-name="sortTotdur"
                                                                        data-control-action="sort"
                                                                        data-path=".totdur"
                                                                        data-order="asc"
                                                                        data-type="number">
                                                                        Duration <i class="fa fa-caret-up"></i>
                                                                    </div>
                                                                </div>

                                                                <div class="large-4 medium-4 small-4 columns colorwht abdd srtarw" onclick="myfunction(this)">
                                                                    <div
                                                                        class="  "
                                                                        data-control-type="sortArrtime"
                                                                        data-control-name="sortArrtime"
                                                                        data-control-action="sort"
                                                                        data-path=".arrtime"
                                                                        data-order="asc"
                                                                        data-type="number">
                                                                        Arrival <i class="fa fa-caret-up"></i>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                            <div class="col-md-2 colorwht abdd centre srtarw hidden-xs" onclick="myfunction(this)">
                                                                <div
                                                                    class=" "
                                                                    data-control-type="sortCITZ1"
                                                                    data-control-name="sortCITZ1"
                                                                    data-control-action="sort"
                                                                    data-path=".price"
                                                                    data-order="asc"
                                                                    data-type="number">
                                                                    Fare <i class="fa fa-caret-up"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="laegr-6 medium-6 small-6 columns">
                                                    <div class="one-side" style="">

                                                        <div class="col-md-12" style="position: relative; top: -4px;">

                                                            <div class="col-md-2 col-xs-4 colorwht abdd lft srtarw" onclick="myfunction(this)">
                                                                <div
                                                                    class=""
                                                                    data-control-type="sortAirlineR"
                                                                    data-control-name="sortAirlineR"
                                                                    data-control-action="sort"
                                                                    data-path=".airlineImage"
                                                                    data-order="asc"
                                                                    data-type="text">
                                                                    Airline <i class="fa fa-caret-up"></i>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-8">
                                                                <div class="large-4 medium-4 small-4 columns colorwht abdd lft srtarw" onclick="myfunction(this)">
                                                                    <div
                                                                        class=""
                                                                        data-control-type="sortDeptimeR"
                                                                        data-control-name="sortDeptimeR"
                                                                        data-control-action="sort"
                                                                        data-path=".deptime"
                                                                        data-order="asc"
                                                                        data-type="number">
                                                                        Depart <i class="fa fa-caret-up"></i>
                                                                    </div>
                                                                </div>
                                                                <div class="large-4 medium-4 small-4 columns colorwht abdd lft srtarw" onclick="myfunction(this)">
                                                                    <div
                                                                        class=""
                                                                        data-control-type="sortArrtimeR"
                                                                        data-control-name="sortArrtimeR"
                                                                        data-control-action="sort"
                                                                        data-path=".arrtime"
                                                                        data-order="asc"
                                                                        data-type="number">
                                                                        Arrival <i class="fa fa-caret-up"></i>
                                                                    </div>
                                                                </div>
                                                                <div class="large-4 medium-4 small-4 columns colorwht abdd lft srtarw r-dep" onclick="myfunction(this)">
                                                                    <div
                                                                        class=""
                                                                        data-control-type="sortTotdurR"
                                                                        data-control-name="sortTotdurR"
                                                                        data-control-action="sort"
                                                                        data-path=".totdur"
                                                                        data-order="asc"
                                                                        data-type="number">
                                                                        Duration <i class="fa fa-caret-up"></i>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2 colorwht abdd  centre srtarw r-fare" onclick="myfunction(this)">
                                                                <div
                                                                    class=""
                                                                    data-control-type="sortCITZR"
                                                                    data-control-name="sortCITZR"
                                                                    data-control-action="sort"
                                                                    data-path=".price"
                                                                    data-order="asc"
                                                                    data-type="number">
                                                                    Fare <i class="fa fa-caret-up"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="clear">
                                                </div>
                                            </div>
                                        </div>




                                    </div>
                                </div>

                                <div class="row">
                                    <div class="laegr-12 medium-12 small-18">
                                        <div class="large-6 medium-6 small-6 columns scr-ol">
                                            <div id="divFrom1" class="listO w100">
                                                <%-- <div class="dvsrc">
                                        <img src="~/Images/fltloding.gif" />
                                    </div>--%>
                                            </div>
                                        </div>
                                        <div class="large-6 medium-6 small-6 columns scr-ol">
                                            <div id="divTo1" class="listR w100">
                                                <%--<div class="dvsrc textaligncenter">
                                        <img src="~/Images/fltloding.gif" />
                                    </div>--%>
                                            </div>

                                            <%--<div class="dvsrc textaligncenter">
                                    <img src="~/Images/fltloding.gif" />
                                </div>--%>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="flightbox" style="display: block;" id="onewayH">
                                <div class="" style="width: 99%;">



                                    <div class="jplist-panel passenger">
                                        <div id="DivLoadP" class="w100" style="margin: 0px 0px 16px 0px; display: none;"><span style="position: absolute; text-align: center; margin: 0 0 0 0; color: #61b3ff; width: 100%; padding: -3px;">We are processing, Please wait....</span></div>
                                        <%-- <div class="clspMatrix lft" id="clspMatrix"><span style="padding-left: 20px;">Matrix</span></div>--%>
                                        <div class="clear"></div>
                                        <div id="divMatrixRtfR" class="divMatrix" style="display: none"></div>
                                        <%--  <div class="clear1"></div>--%>
                                        <div id="divMatrix"></div>
                                        <%--<div class="clear1"></div>--%>
                                        <div class="w100 auto passenger">
                                            <div class="w100 ptop10" style="background: #005999; padding: 1px 18px 0px 0px; border-radius: 4px; overflow: hidden; min-height: 60px; border: 1px solid #ccc; width: 99%;">
                                                <div class="lft" style="margin-top: 16px;">


                                                    <div id="displaySearchinput" class="lft"></div>
                                                    <div class="lft" style="font-size: 12px; color: #fff; position: relative; top: 4px;">
                                                        <span style="color: #fff;">&nbsp;&nbsp;|</span>  <span id="spanShow" onclick="ShowHideDiscount('show');" class="spnBtnShow" style="cursor: pointer; color: #fff; z-index: 1011;">SHOW NET</span>
                                                        <span id="spanHide" onclick="ShowHideDiscount('hide');" style="display: none; cursor: pointer; color: #fff;" class="spnBtnHide">HIDE NET</span>
                                                    </div>
                                                    <%-- <div class="lft plft10">
                                                        <div class="lft ">
                                                            Del<br />
                                                            <span class="f10 txtgray">Fri 7 Mar</span>
                                                        </div>
                                                        <div class="lft">
                                                            <img src="../Images/rarrow.png" /></div>
                                                        <div class="lft plft10">
                                                            Bom<br />
                                                            <span class="f10 txtgray">Fri 7 Mar </span>
                                                        </div>


                                                    </div>
                                                    <div class="bdrdot lft">&nbsp;</div>

                                                    <div class="lft plft10">
                                                        <div class="lft ">
                                                            Del<br />
                                                            <span class="f10 txtgray">Fri 7 Mar</span>
                                                        </div>
                                                        <div class="lft">
                                                            <img src="../Images/arrow.png" /></div>
                                                        <div class="lft plft10">
                                                            Bom<br />
                                                            <span class="f10 txtgray">Fri 7 Mar </span>
                                                        </div>


                                                    </div>
                                                    <div class="bdrdot lft">&nbsp;</div>

                                                    <div class="lft plft10">
                                                        <div class="lft ">
                                                            Del<br />
                                                            <span class="f10 txtgray">Fri 7 Mar</span>
                                                        </div>
                                                        <div class="lft">
                                                            <img src="../Images/arrow.png" /></div>
                                                        <div class="lft plft10">
                                                            Bom<br />
                                                            <span class="f10 txtgray">Fri 7 Mar </span>
                                                        </div>


                                                    </div>
                                                    <div class="bdrdot lft">&nbsp;</div>



                                                    <div class="lft plft10">
                                                        <div class="lft ">
                                                            Del<br />
                                                            <span class="f10 txtgray">Fri 7 Mar</span>
                                                        </div>
                                                        <div class="lft">
                                                            <img src="../Images/arrow.png" /></div>
                                                        <div class="lft plft10">
                                                            Bom<br />
                                                            <span class="f10 txtgray">Fri 7 Mar </span>
                                                        </div>


                                                    </div>--%>
                                                </div>
                                                <%--<div id="RTFSAirMain" class="hide box-return">--%>
                                                <div id="RTFSAirMain" class="box-return" style="display: none;">
                                                    <div class="w15 lft">&nbsp;</div>
                                                    <div class="bld underlineitalic colormn lft" style="display: none;">Special Return Fares</div>
                                                    <div id="splLoading">Loading......</div>
                                                    <div class="clear"></div>
                                                    <div id="RTFSAir">Loading.....</div>
                                                    <div class="clear"></div>
                                                </div>

                                                <div class="rgt" style="margin-top: 20px; color: #000;"><span class="hidden-md hidden-lg hidden-sm fliters " onclick="openNav()"><i class="fa fa-filter fa-2x" aria-hidden="true" style="color: #fff;"></i></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span><a href="#" id="show" onclick="openMod()"><i class="fa fa-edit fa-2x" style="color: #fff;"></i></a></span></div>

                                                <div class="rgt passenger-3" id="prexnt" style="margin-top: 20px; color: #000;">

                                                    <div class="rgt">
                                                        <%--<input type="button" id="NextDay" value="Next Day" class="pnday daybtn f10" />--%>
                                                        <div class="auto lft">
                                                            <a href="#" id="PrevDay" class="pnday" style="color: #fff">
                                                                <img src="../Styles/images/closeopen2.png" />&nbsp;Prev Day</a>
                                                        </div>
                                                        <div class="dotbdr lft">&nbsp;</div>
                                                        <div class="auto rgt"><a href="#" id="NextDay" class="pnday" style="color: #fff">Next Day &nbsp;<img src="../Styles/images/closeopen.png" /></a></div>
                                                        <%-- <img src="../Styles/images/closeopen.png" />--%>

                                                        <%-- <img src="../Styles/images/closeopen2.png" />--%>
                                                        <%-- <input type="button" id="PrevDay" value="Prev Day" class="pnday daybtn f10" />--%>
                                                    </div>
                                                </div>

                                            </div>



                                            <%--<div class="lft padding1s cursorpointer clspMatrix" id="clspMatrix"></div>--%>


                                            <%-- <div class="lft w48" id="displaySearchinput">sdasd</div>--%>
                                        </div>
                                    </div>



                                </div>
                                <div class="nav-container">
                                    <div class="nav">

                                        <div class="jplist-panel">

                                            <div class="headerow">
                                                <div class="large-2 hidden-xs medium-2 small-3 columns colorwht abdd srtarw" onclick="myfunction(this)">
                                                    <div class=""
                                                        data-control-type="sortAirline"
                                                        data-control-name="sortAirline"
                                                        data-control-action="sort"
                                                        data-path=".airlineImage"
                                                        data-order="asc"
                                                        data-type="text">
                                                        Airline <i class="fa fa-caret-up"></i>
                                                    </div>
                                                </div>

                                                <div class="large-2 medium-2 small-3 columns colorwht abdd srtarw" onclick="myfunction(this)">

                                                    <div
                                                        class=""
                                                        data-control-type="sortDeptime"
                                                        data-control-name="sortDeptime"
                                                        data-control-action="sort"
                                                        data-path=".deptime"
                                                        data-order="asc"
                                                        data-type="number" style="margin-left: -39px;">
                                                        Departure <i class="fa fa-caret-up"></i>
                                                    </div>
                                                </div>

                                                <div class="large-2 medium-2 small-3 columns colorwht abdd srtarw" onclick="myfunction(this)">

                                                    <div
                                                        class=""
                                                        data-control-type="sortArrtime"
                                                        data-control-name="sortArrtime"
                                                        data-control-action="sort"
                                                        data-path=".arrtime"
                                                        data-order="asc"
                                                        data-type="number" style="margin-left: -39px;">
                                                        Arrival <i class="fa fa-caret-up"></i>
                                                    </div>

                                                </div>



                                                <div class="large-2 medium-2 small-4 columns passenger abdd colorwht  srtarw" onclick="myfunction(this)">

                                                    <div
                                                        class=""
                                                        data-control-type="sortTotdur"
                                                        data-control-name="sortTotdur"
                                                        data-control-action="sort"
                                                        data-path=".totdur"
                                                        data-order="asc"
                                                        data-type="number" style="margin-left: -83px;">
                                                        Duration <i class="fa fa-caret-up"></i>
                                                    </div>
                                                </div>



                                                <div class="large-2 medium-2 small-3 columns abdd colorwht srtarw hidden-xs " onclick="myfunction(this)">
                                                    <div
                                                        class=""
                                                        data-control-type="sortCITZ"
                                                        data-control-name="sortCITZ"
                                                        data-control-action="sort"
                                                        data-path=".price"
                                                        data-order="asc"
                                                        data-type="number" style="margin-left: -76px;">
                                                        Fare <i class="fa fa-caret-up"></i>
                                                    </div>
                                                </div>
                                                <div class="clear">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div id="mainDiv">
                                    <div id="divResult" class="list">
                                        <div id="divFrom" class="list" style="width: 100%;">
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>


                                <%-- <div class="jplist-no-results jplist-hidden">
                                <div class='clear1'></div>
                                <div class='clear1'></div>
                                <div class='w90 mauto padding1 brdr'>
                                    <div class='clear1'></div>
                                    <div class='clear1'></div>
                                    <span class='vald f20'>Sorry, we could not find a match for your query. Please modify your search.</span> &nbsp;<span onclick='DiplayMsearch(this.id);' class='underlineitalic cursorpointer'>Modify Search</span><div class='clear'></div>
                                </div>
                            </div>--%>
                            </div>
                        </div>

                        <div class="clear">
                        </div>
                    </div>
                </div>
                <div class="clear">
                </div>
            </div>
        </div>
        <div class="clear">
        </div>
        <div id="render">
        </div>
        <div class="clear">
        </div>
    </div>

    <%--<div id="fltselct" class="hide">
        <div class="clear1"></div>
        <div id="totalPay" class="f16">
            Your Selection
            <div class="clear1"></div>
        </div>
        <div id="selctcntnt" class="w70 mauto">
            <div class="clear1">
            </div>
            <div id="fltgo" class="w50 lft">
            </div>
            <div id="fltbk" class="w45 rgt">
            </div>
            <div id="fltbtn">
                <input type="button" value="Book" class="button1 hide" id="FinalBook" />
                <div id="Divproc" class="hide bld">
                    <img alt="Booking In Progress" src="~/Images/loading_bar.gif" />
                </div>
            </div>
        </div>
    </div>--%>
    <div id="waitMessage" style="display: none;">
        <div style="text-align: center; z-index: 101111111111111; font-size: 12px; font-weight: bold; padding: 20px;">

            <div id="loader" style="display: none;">

                <div id="plane">
                    <i>
                        <img src="../Images/icons/146552.png" style="width: 60px;" /></i>
                </div>

            </div>





            <svg xmlns="http://www.w3.org/2000/svg" version="1.1">
                <defs>
                    <filter id="gooey">
                        <feGaussianBlur in="SourceGraphic" stdDeviation="10" result="blur"></feGaussianBlur>
                        <feColorMatrix in="blur" mode="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 18 -7" result="goo"></feColorMatrix>
                        <feBlend in="SourceGraphic" in2="goo"></feBlend>
                    </filter>
                </defs></svg>
            <div class="blob blob-0"></div>
            <div class="blob blob-1"></div>
            <div class="blob blob-2"></div>
            <div class="blob blob-3"></div>
            <div class="blob blob-4"></div>
            <div class="blob blob-5"></div>

            <div class="animation" style="display: none;">

                <svg id="master-artboard" class="animation__cloud--back" viewBox="0 0 45 18" version="1.1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="45px" height="18px">
                    <path d="M 58.88372802734375 825.155517578125 C 16.61685562133789 826.1433715820312 57.925209045410156 781.6769409179688 58.883731842041016 781.3507080078125 C 106.25779724121094 731.152099609375 169.17739868164062 692.9862670898438 226.64694213867188 694.6730346679688 C 227.2968292236328 557.091552734375 389.74322509765625 563.0558471679688 425.166748046875 635.9559326171875 C 534.7359619140625 431.2034912109375 793.6226196289062 599.6361694335938 715.956298828125 741.27392578125 C 820.5570068359375 803.94287109375 789.773193359375 826.9736328125 767.21728515625 825.1555786132812 C 394.85125732421875 825.5911865234375 359.5561218261719 823.805908203125 58.88372802734375 825.155517578125 Z" transform="matrix(0.0573558509349823, 0, 0, 0.056244850158691406, -1.3596858978271484, -29.666284561157227)" />
                </svg>
                <svg id="Svg1" class="animation__cloud--middle" viewBox="0 0 45 18" version="1.1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="45px" height="18px">
                    <path d="M 58.88372802734375 825.155517578125 C 16.61685562133789 826.1433715820312 57.925209045410156 781.6769409179688 58.883731842041016 781.3507080078125 C 106.25779724121094 731.152099609375 169.17739868164062 692.9862670898438 226.64694213867188 694.6730346679688 C 227.2968292236328 557.091552734375 389.74322509765625 563.0558471679688 425.166748046875 635.9559326171875 C 534.7359619140625 431.2034912109375 793.6226196289062 599.6361694335938 715.956298828125 741.27392578125 C 820.5570068359375 803.94287109375 789.773193359375 826.9736328125 767.21728515625 825.1555786132812 C 394.85125732421875 825.5911865234375 359.5561218261719 823.805908203125 58.88372802734375 825.155517578125 Z" transform="matrix(0.0573558509349823, 0, 0, 0.056244850158691406, -1.3596858978271484, -29.666284561157227)" />
                </svg>
                <div class="animation__plane--shadow"></div>

                <svg class="animation__plane" xmlns="http://www.w3.org/2000/svg" xmlns:cc="http://creativecommons.org/ns#" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:svg="http://www.w3.org/2000/svg" data-name="svgi-plane" viewBox="0 0 135.67 49.55">
                    <path fill="#fff" stroke="#464646" stroke-linejoin="round" d="M74.663 45.572h-9.106z" class="svgi-plane--stripe3" />
                    <path fill="#fff" stroke="#464646" stroke-linejoin="round" stroke-width="1" d="M.75 26.719h23.309z" class="svgi-plane--stripe1" />
                    <path fill="#fff" stroke="#464646" stroke-linejoin="round" stroke-width="1" d="M11.23 31.82h22.654z" class="svgi-plane--stripe2" />
                    <path fill="#fff" stroke="#464646" stroke-linejoin="round" stroke-width="1" d="m 53.47597,24.263013 h 68.97869 c 6.17785,0 12.47074,6.758518 12.40872,8.67006 -0.05,1.537903 -5.43763,7.036166 -11.72452,7.056809 l -59.599872,0.201269 c -9.092727,0.03097 -23.597077,-5.992662 -22.178652,-11.794378 1.160348,-4.74789 7.862358,-4.13376 12.115634,-4.13376 z" />
                    <path fill="#fff" stroke="#464646" stroke-linejoin="round" stroke-width="1" d="M 45.243501,24.351777 37.946312,0.952937 h 7.185155 c 15.37061,20.184725 28.402518,23.28324 28.402518,23.28324 0,0 -27.106129,-0.178562 -28.290484,0.1156 z" />
                    <path fill="#fff" stroke="#464646" stroke-linejoin="round" stroke-width="1" d="m 42.699738,18.984597 h 10.627187 c 5.753726,0 5.364609,7.799958 0,7.799958 H 42.998828 c -4.96749,0 -5.574672,-7.799958 -0.29909,-7.799958 z m 33.139939,16.164502 h 29.656893 c 6.62199,0 6.49395,6.577892 0,6.577892 H 75.940707 c -8.658596,0 -8.499549,-6.35598 -0.10103,-6.577892 z m 9.693907,6.664592 h 8.86866 c 4.439332,0 4.309293,7.066099 0,7.066099 h -8.756626 z" />
                    <path fill="#fff" stroke="#464646" stroke-linejoin="round" stroke-width="1" d="m 85.55159,42.447431 c 0,0 -5.282585,0.456211 -6.372912,3.263659 1.335401,2.378073 6.397919,2.528767 6.397919,2.528767 z" />
                    <path fill="#fff" stroke="#464646" stroke-linejoin="round" stroke-width="1" d="m 133.68903,31.07417 h -7.01411 c -1.26938,0 -2.89286,-1.005314 -3.21496,-3.216179 h 7.50225 z" />
                    <ellipse cx="113.564" cy="29.448534" fill="#fff" stroke="#464646" stroke-linecap="square" stroke-linejoin="round" stroke-width="1" rx="1.3354006" ry="1.6400863" />
                    <ellipse cx="107.56219" cy="29.448534" fill="#fff" stroke="#464646" stroke-linecap="square" stroke-linejoin="round" stroke-width="1" rx="1.3354006" ry="1.6400863" />
                    <ellipse cx="101.56039" cy="29.448534" fill="#fff" stroke="#464646" stroke-linecap="square" stroke-linejoin="round" stroke-width="1" rx="1.3354006" ry="1.6400863" />
                    <ellipse cx="95.558594" cy="29.448534" fill="#fff" stroke="#464646" stroke-linecap="square" stroke-linejoin="round" stroke-width="1" rx="1.3354006" ry="1.6400863" />
                    <ellipse cx="89.556793" cy="29.448534" fill="#fff" stroke="#464646" stroke-linecap="square" stroke-linejoin="round" stroke-width="1" rx="1.3354006" ry="1.6400863" />
                    <ellipse cx="83.554993" cy="29.448534" fill="#fff" stroke="#464646" stroke-linecap="square" stroke-linejoin="round" stroke-width="1" rx="1.3354006" ry="1.6400863" />
                    <ellipse cx="77.553192" cy="29.448534" fill="#fff" stroke="#464646" stroke-linecap="square" stroke-linejoin="round" stroke-width="1" rx="1.3354006" ry="1.6400863" />
                    <ellipse cx="71.551392" cy="29.448534" fill="#fff" stroke="#464646" stroke-linecap="square" stroke-linejoin="round" stroke-width="1" rx="1.3354006" ry="1.6400863" />
                    <ellipse cx="65.549591" cy="29.448534" fill="#fff" stroke="#464646" stroke-linecap="square" stroke-linejoin="round" stroke-width="1" rx="1.3354006" ry="1.6400863" />
                </svg>
                <svg id="Svg2" class="animation__cloud--front" viewBox="0 0 45 18" version="1.1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="45px" height="18px">
                    <path d="M 58.88372802734375 825.155517578125 C 16.61685562133789 826.1433715820312 57.925209045410156 781.6769409179688 58.883731842041016 781.3507080078125 C 106.25779724121094 731.152099609375 169.17739868164062 692.9862670898438 226.64694213867188 694.6730346679688 C 227.2968292236328 557.091552734375 389.74322509765625 563.0558471679688 425.166748046875 635.9559326171875 C 534.7359619140625 431.2034912109375 793.6226196289062 599.6361694335938 715.956298828125 741.27392578125 C 820.5570068359375 803.94287109375 789.773193359375 826.9736328125 767.21728515625 825.1555786132812 C 394.85125732421875 825.5911865234375 359.5561218261719 823.805908203125 58.88372802734375 825.155517578125 Z" transform="matrix(0.0573558509349823, 0, 0, 0.056244850158691406, -1.3596858978271484, -29.666284561157227)" />
                </svg>
                <div class="animation__loader"></div>


            </div>

            <footer>

    <div id="searchquery" style="color: #fff;  font-size: 18px;z-index:10111111;" >
                </div>

 <%-- <a href="https://www.linkedin.com/in/giovanni-mrqs/" target="_blank">linkedin</a>
  -
  <a href="https://github.com/web-duke" target="_blank">github</a>
  -
  <a href="https://dock.io?r=dukeweb:aaaat3HE" target="_blank">dock.io</a>
  <br>
  <span>buy me a coffee with bitcoin&nbsp;: 1CVRSirjLkKQdY2prdBta2udcoEYCKEwms</span>--%>
</footer>



        </div>
    </div>





    <%-- <div id="divMail">
        <a href="#" class="topopup pop_button1" id="btnFullDetails">Mail All Result</a>
        <a href="#" class="topopup pop_button1" id="btnSendHtml">Mail Selected Result</a>
    </div>--%>
    <div id="backgroundPopup">
    </div>
    <div id="toPopup" class="flight_head">
        <div class="close">
        </div>
        <span class="ecs_
            ">Press Esc to close <span class="arrow"></span></span>
        <div id="popup_content">
            <!--your content start-->
            <table cellpadding="3" cellspacing="3">
                <tr>
                    <td colspan="2">
                        <h4 style="text-align: center; color: #FFFFFF; background-color: #20313f; font-weight: bold; padding-top: 5px; padding-bottom: 5px;">Send Mail</h4>
                    </td>
                </tr>
                <tr>
                    <td class="textsmall" style="width: 120px; padding-left: 10px;"></td>
                    <td class="textsmall">
                        <input type='radio' name='choices' checked="checked" value='A' />
                        All Result
                         <input type='radio' name='choices' value='S' />Selected Result
                    </td>
                </tr>

                <tr>
                    <td class="textsmall" style="width: 120px; padding-left: 10px;">From:
                    </td>
                    <td>
                        <input type="text" class="headmail" id="txtFromMail" name="txtFromMail" />
                    </td>
                </tr>
                <tr>
                    <td class="textsmall" style="width: 120px; padding-left: 10px;">To:
                    </td>
                    <td>
                        <input type="text" class="headmail" id="txtToMail" name="txtToMail" />
                    </td>
                </tr>
                <tr>
                    <td class="textsmall" style="width: 120px; padding-left: 10px;">Subject:
                    </td>
                    <td>
                        <input type="text" class="headmail" id="txtSubj" name="txtSubj" />
                    </td>
                </tr>
                <tr>
                    <td class="textsmall" style="width: 120px; padding-left: 10px;">Message:
                    </td>
                    <td>
                        <textarea id="txtMsg" class="headmail" name="txtMsg" rows="4" cols="20"></textarea>
                    </td>
                </tr>
                <tr>
                    <td style="margin-left: 20px;"></td>
                    <td align="right">
                        <%--<input type="button" class="pop_button" id="btnCancel" name="btnCancel" value="Cancel" />--%>
                        <input type="button" class="buttonfltbk" id="btnSendMail" name="btnSendMail" value="Send Details" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <%--<div id="divabc">&nbsp;</div>--%>
                        <label id="lblMailStatus" style="display: none; color: Red;">
                        </label>
                    </td>
                </tr>
            </table>
        </div>
        <!--your content end-->
    </div>
    <div id="FareBreakupHeder" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="FareBreakupHederLabel" aria-hidden="true" style="display: none;">



        <div class="modal-dialog" role="document" style="height: 400px; overflow-x: scroll; top: 2px; background: #fff; border-radius: 4px; -webkit-box-shadow: 0 2px 0 0px rgba(0,0,0,.08); -moz-box-shadow: 0 2px 0 0px rgba(0,0,0,.08); border: 1px solid #dad8d8;">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="FareBreakupHederLabel">Fare Breakup</h5>
                    <button type="button" class="close FareBreakupHederClose" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>

                </div>
                <div class="modal-body" id="FareBreakupHederId" style="padding: 2px 10px">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary FareBreakupHederClose">Close</button>
                </div>
            </div>
        </div>

    </div>

    <div id="ConfmingFlight" class="CfltFare" style="display: none;">
        <div id="divLoadcf" class="">
        </div>
    </div>



    <div class="clear"></div>
    <%--    <a href="#toptop"><span class="toptop" style="position: fixed; bottom: 4px; right: 20px; height: 50px; font-size: 20px; width: 50px; border-radius: 50%; cursor: pointer; padding: 13px 15px; background: rgb(0, 75, 145); color: rgb(255, 255, 255); display: block;"><i class="fa fa-chevron-up" aria-hidden="true"></i></span></a>--%>
    <div class="clear1"></div>
    <input type="hidden" id="hdnMailString" name="hdnMailString" />
    <input type="hidden" id="hdnAllOrSelecte" name="hdnAllOrSelecte" />
    <input type="hidden" id="hdnOnewayOrRound" name="hdnOnewayOrRound" />
    <asp:Literal ID="henAgcDetails" runat="server" Visible="false"></asp:Literal>
    <input type="hidden" id="hdnSRFPriceLineNoO" name="hdnSRFPriceLineNoO" />
    <input type="hidden" id="hdnSRFAircodeO" name="hdnSRFAircodeO" />
    <input type="hidden" id="hdnSRFPriceLineNoR" name="hdnSRFPriceLineNoR" />
    <input type="hidden" id="hdnSRFAircodeR" name="hdnSRFAircodeR" />

    <input type="hidden" id="hdnSRFPriceLineNoO" name="hdnSRFPriceLineNoO" />
    <input type="hidden" id="hdnSRFAircodeO" name="hdnSRFAircodeO" />
    <input type="hidden" id="hdnSRFPriceLineNoR" name="hdnSRFPriceLineNoR" />
    <input type="hidden" id="hdnSRFAircodeR" name="hdnSRFAircodeR" />
    <script type="text/javascript">
        <%--var UrlBase = '<%=ResolveUrl("~/")%>';--%>
        //var UrlBase = window.location.protocol + "//" + location.host + "/";
    </script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.7.1.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/json2.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/Flight/JSLINQ.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jplist.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/Flight/SortAD.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/Flight/TextFilterGroup.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/Flight/handleQueryString.js?v=3.9")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/Flight/jquery.blockUI.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/Flight/async.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/Flight/LZCompression.js?y=9.8")%>"></script>
    <%--<script type="text/javascript" src="<%=ResolveUrl("~/Scripts/Flight/FlightResultsNew2.1.js?v=1.7")%>"></script>--%>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/Flight/FlightResultsNew.js?v=1")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery.tooltip.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/gridview-readonly-script.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/Flight/FlightMailing.js") %>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/Flight/pako.min.js") %>"></script>

    <%--   <script src="../Scripts/script.js" type="text/javascript"></script>--%>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#hide").click(function () {
                $(".modsearch").hide();
            });
            $("#show").click(function () {
                $("modsearch").show();
            });
        });
    </script>


    <script type="text/javascript">
        function openNav() {

            document.getElementById("mysidenavssss").style.width = "375px";
            document.getElementById("passengersss").style.display = "block";

        }

        function closeNav() {
            document.getElementById("mysidenavssss").style.width = "0";
            document.getElementById("passengersss").style.display = "none";
        }


        function openMod() {


            document.getElementById("mymod").style.width = "375px";
            document.getElementById("dismod").style.display = "block";

        }

        function closeMod() {
            document.getElementById("mymod").style.width = "0";
            document.getElementById("dismod").style.display = "none";
        }

    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".detls1").click(function () {
                $(".detls").toggle();
            });
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $(".abc1").unbind('click').bind('click', function (e) {

                if (e.target.id != "CheckboxT1") {
                    $(this).toggleClass("bdrss");
                }
            });
            $(".abc2t").unbind('click').bind('click', function (e) {

                if (e.target.id != "CheckboxT2") {
                    $(this).toggleClass("bdrss");
                }
            });
            $(".abc3t").unbind('click').bind('click', function (e) {

                if (e.target.id != "CheckboxT3") {
                    $(this).toggleClass("bdrss");
                }

            });
            $(".abc4t").unbind('click').bind('click', function (e) {

                if (e.target.id != "CheckboxT4") {
                    $(this).toggleClass("bdrss");
                }

            });

        });
    </script>


    <script type="text/javascript">

        debugger;
        var urlParams = new URLSearchParams(location.search);

        //  urlParams.has('type');  // true
        // urlParams.get('NStop');    // 1234
        var NStop = urlParams.get('NStop'); //GetQueryStringParams('NStop')
        //function GetQueryStringParams(sParam)
        //{
        //    debugger;
        //        var sPageURL = window.location.search.substring(1);
        //        var sURLVariables = sPageURL.split('&');
        //        for (var i = 0; i < sURLVariables.length; i++)
        //        {
        //            var sParameterName = sURLVariables[i].split('=');
        //            if (sParameterName[0] == sParam)
        //            {
        //                return sParameterName[1];
        //            }
        //        }
        //    }​




        if (NStop == "TRUE") {
            $('#dsplm').hide();
        }


        $("p:odd").removeClass("blue under");
    </script>

    <script type="text/javascript">

        jQuery("document").ready(function ($) {
            debugger;

            //$(".onewayss").removeClass("col-md-4 nopad text-search mltcs").addClass("col-md-5 nopad text-search mltcs");

            //$("#rdbOneWay").click(function () {

            //    $(".onewayss").removeClass("col-md-4 nopad text-search mltcs").addClass("col-md-5 nopad text-search mltcs");
            //    $(".sk").removeAttr(disabled = "disabled");
            //});

            //$("#rdbRoundTrip").click(function () {

            //    $(".onewayss").removeClass("col-md-5 nopad text-search mltcs").addClass("col-md-4 nopad text-search mltcs");

            //});
            //$("#rdbMultiCity").click(function () {
            //    $(".onewayss").removeClass("col-md-4 nopad text-search mltcs").addClass("col-md-5 nopad text-search mltcs");
            //});

            var nav = $('.nav-container');
            $(window).scroll(function () {
                if ($(this).scrollTop() > 175) {
                    $(".toptop").fadeIn();
                    if ($("#lftdv1").is(":visible") == false) {
                        nav.addClass("f-nav1");
                    }
                    else {
                        nav.addClass("f-nav");
                    }
                } else {
                    $(".toptop").fadeOut();
                    nav.removeClass("f-nav");
                    nav.removeClass("f-nav1");
                }
            });

            var Triptype = getUrlVars()["TripType"];
            //if (Triptype == "rdbOneWay") {
            //    $(".onewayss").removeClass("col-md-4 nopad text-search mltcs").addClass("col-md-5 nopad text-search mltcs");
            //}



            //if (Triptype == "rdbOneWay") {
            //    $(".onewayss").removeAttr(disable = "disable");
            //}

            //if (Triptype == "rdbRoundTrip") {
            //    $(".onewayss").removeAttr(disable = "disable");
            //}


            //if (Triptype == "rdbRoundTrip") {
            //    $(".onewayss").removeClass("col-md-5 nopad text-search mltcs").addClass("col-md-4 nopad text-search mltcs");
            //}
        });
        function getUrlVars() {
            var vars = [], hash;
            var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
            for (var i = 0; i < hashes.length; i++) {
                hash = hashes[i].split('=');
                vars.push(hash[0]);
                vars[hash[0]] = hash[1];
            }
            return vars;
        }
    </script>
    <%--<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>--%>
</asp:Content>
