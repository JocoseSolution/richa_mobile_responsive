﻿<%--<%@ Page Language="VB" AutoEventWireup="false" CodeFile="AgentTypeDetails.aspx.vb" Inherits="SprReports_Agent_AgentTypeDetails" %>--%>

<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageForDash.master" AutoEventWireup="false" CodeFile="TypeDetailsRefresh.aspx.vb" Inherits="SprReports_Agent_AgentTypeDetails" %>

<%@ Register Src="~/UserControl/Settings.ascx" TagPrefix="uc1" TagName="Settings" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


      <script language="javascript" type="text/javascript">
          function validateSearch() {

              if (document.getElementById("ctl00_ContentPlaceHolder1_ddl_ptype").value == "ALL") {
                  alert('Specify Group Type');
                  document.getElementById("ctl00_ContentPlaceHolder1_ddl_ptype").focus();
                  return false;
              }

          }

    </script>







   
       
        <div class="container">
          
                 <div class="card-header">
            <div class="col-md-9">
                <h3 style="text-align: center;color:orange" >Group Type Details</h3>
                <hr style="height:3px;background:orange" />
            </div>
        </div>
           
<div class="card-body">

        <asp:Label ID="LableMsg" runat="server" Font-Bold="true" ForeColor="Red"></asp:Label>
        <div class="col-md-9">

            <div class="row">
           <div class="col-md-5">
                <label>Group Type</label>
              
                      <asp:DropDownList ID="ddl_ptype" CssClass="form-control" runat="server"  AppendDataBoundItems="true">
                      </asp:DropDownList>
                    <asp:TextBox ID="TextBoxGroupType" Style="display:none" runat="server"></asp:TextBox>
         <asp:RequiredFieldValidator ID="RequiredFieldValidatorType" runat="server" ErrorMessage="Group Type is Required." ControlToValidate="TextBoxGroupType" ValidationGroup="ins"></asp:RequiredFieldValidator>
                </div>
                 
           
                <div class="col-md-3">
                    <label></label>
                    <asp:Button runat="server" CssClass="btn btn-danger" ID="ButtonSubmit" OnClientClick="return validateSearch()" Text="Insert" style="    margin-top: 23px;"/>
                </div>
                </div>
 

            <div class="row">
                <div style="color:#000;font-weight:bolder"> Only,New Records are Insert and Nothing Do in Existing Record</div>
            </div>
           
        </div>

  </div>
       
     
    </div>
</asp:Content>
