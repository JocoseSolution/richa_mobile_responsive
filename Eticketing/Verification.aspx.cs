﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using IRCTC_RDS;
using System.Configuration;
using System.IO;

public partial class Eticketing_Verification : System.Web.UI.Page
{
    EncryptDecryptString ENDE = new EncryptDecryptString();
    IrctcRdsPayment RDS = new IrctcRdsPayment();
    protected void Page_Load(object sender, EventArgs e)
    {
        string RDSRequest = string.Empty;
        string IPAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
        if (IPAddress == "" || IPAddress == null)
            IPAddress = Request.ServerVariables["REMOTE_ADDR"];
        if (!IsPostBack)
        {
            string EncryptedReq = string.Empty;
            string DecryptedReq = string.Empty;
            string ReturnUrl = string.Empty;

            string Request_Url_AbsoluteUri = string.Empty;
            string Request_Url_OriginalString = string.Empty;
            string Request_UrlReferrer_AbsoluteUri = string.Empty;
            string Request_UrlReferrer_OriginalString = string.Empty;

            try
            {
                //ReturnUrl = "https://www.agent.irctc.co.in/eticketing/BankResponse";
                RDSRequest = Request.Form.ToString();
                EncryptedReq = Request.Form["encdata"];//Request.Form.ToString();
                                                       //ReturnUrl = "https://www.agent.irctc.co.in/eticketing/BankResponse";
                                                       //ReturnUrl = Convert.ToString(Request.UrlReferrer);

                #region Get Return URL

                #region Get URL From  Request.Url
                try
                {                   
                    Request_Url_AbsoluteUri = Convert.ToString(Request.Url.AbsoluteUri);                   
                }
                catch (Exception ex)
                {
                    int flag1 = RDS.InsertExceptionLog("RDS-IRCTC", "Verification.aspx.cs", "Check Response", "RETURNURL1", Request.Form["encdata"], "Url_AbsoluteUri" + ReturnUrl + "~~" + Request.Form.ToString());
                }
                try
                {
                    Request_Url_OriginalString = Convert.ToString(Request.Url.OriginalString);                    
                }
                catch (Exception ex)
                {
                    int flag1 = RDS.InsertExceptionLog("RDS-IRCTC", "Verification.aspx.cs", "Check Response", "RETURNURL2", Request.Form["encdata"], "Url_OriginalString" + ReturnUrl + "~~" + Request.Form.ToString());
                }
                #endregion #region Get URL From  Request.Url

                #region Get URL From  Request.UrlReferrer
                try
                {
                    if (string.IsNullOrEmpty(ReturnUrl.Trim()))
                    {
                        ReturnUrl = Convert.ToString(Request.UrlReferrer.AbsoluteUri);
                    }
                    Request_UrlReferrer_AbsoluteUri = Convert.ToString(Request.UrlReferrer.AbsoluteUri);                    
                }
                catch (Exception ex)
                {
                    int flag1 = RDS.InsertExceptionLog("RDS-IRCTC", "Verification.aspx.cs", "Check Response", "RETURNURL3", Request.Form["encdata"], "UrlReferrer_AbsoluteUri" + ReturnUrl + "~~" + Request.Form.ToString());
                }
               
                try
                {
                    if (string.IsNullOrEmpty(ReturnUrl.Trim()))
                    {
                        ReturnUrl = Convert.ToString(Request.UrlReferrer.OriginalString);
                    }
                    Request_UrlReferrer_OriginalString = Convert.ToString(Request.UrlReferrer.OriginalString);
                }
                catch (Exception ex)
                {
                    int flag1 = RDS.InsertExceptionLog("RDS-IRCTC", "Verification.aspx.cs", "Check Response", "RETURNURL4", Request.Form["encdata"], "UrlReferrer_OriginalString" + ReturnUrl + "~~" + Request.Form.ToString());
                }
                #endregion Get URL From  Request.UrlReferrer
                
                try
                {
                    if (string.IsNullOrEmpty(ReturnUrl.Trim()))
                    {
                        ReturnUrl = "https://www.agent.irctc.co.in/eticketing/BankResponse";
                    }                    
                    File.AppendAllText("C:\\\\CPN_SP\\\\DV_RUURL_RDS" + System.DateTime.Now.Date.ToString("ddMMyyyyhhhmmsstt") + ".txt", System.DateTime.Now.ToString() + Environment.NewLine + Environment.NewLine + "Request_Url_AbsoluteUri:" +  Request_Url_AbsoluteUri + Environment.NewLine +  "Request_Url_OriginalString:" +  Request_Url_OriginalString + Environment.NewLine + "Request_UrlReferrer_AbsoluteUri:" + Request_UrlReferrer_AbsoluteUri + Environment.NewLine + "Request_UrlReferrer_OriginalString:" + Request_UrlReferrer_OriginalString + Environment.NewLine + "ReturnUrl:" + ReturnUrl+ Environment.NewLine + Environment.NewLine);
                }
                catch (Exception ex)
                {

                }
                #endregion Get Return URL


                int flag = RDS.InsertExceptionLog("RDS-IRCTC", "Verification.aspx.cs", "Check Response", "RDSVERIFICATION1", Request.Form["encdata"], "IRCTC_ReturnUrl-" + ReturnUrl+"~~"+ Request.Form.ToString());
            }
            catch(Exception ex)
            {
                int flag = RDS.InsertExceptionLog("RDS-IRCTC", "Verification.aspx.cs", "Check Response", "RDSVERIFICATION1_EXC", ex.Message + "Request_Form" + Request.Form.ToString(), "IRCTC_ReturnUrl-" + ReturnUrl +",Encdata: "+ EncryptedReq + "~~" + ex.StackTrace.ToString());
            }

            try
            {
               // EncryptedReq = "4E051A242D1FC1DDD5A4F3BCEF583C6E556B9464BBB368C52C180DE3555DA304A8452C649254ED500CDF4BB3DDDE4AF826BCDF07B2BCBFA6B9C42503F9C5E2B22E21D9ED073FF7A303FB0BE4CF1096127FDA79CD01C7FCE187E591FE915557EF47171E5C0121CC373A46FFAA74F58331B123BFDD17B345ECADE529E27FFA74121E623DA934EB05CA57BF70DC925C501845FA82FC88A934D462869E5A4AE3A5FAA9646705FED3F7CF3E045C451294B464";

            //if (RDSRequest.Contains("encdata=") && RDSRequest.Split('=').Length > 1)
            //{
                if (!string.IsNullOrEmpty(EncryptedReq))
            {
                    //EncryptedReq = RDSRequest.Split('=')[1];
                    //DecryptedReq = ENDE.DecryptString(EncryptedReq, "abfb7c8d48dfc4f1ce7ed92a44989f25");
                    //string EnDecryptKey = Convert.ToString(ConfigurationManager.AppSettings["RDSKEY"]);
                    //ReturnUrl = GetReturnUrlForDoubleVerification(ReferenceNo, merchantCode, reservationId, bankTxnId);  //Get Return URL from  RDSRequest table 
                    DecryptedReq = ENDE.DecryptString(EncryptedReq);

                //if (DecryptedReq.Contains("merchantCode=IR_") == false)
                //{
                //    DecryptedReq = "merchantCode=IR_" + DecryptedReq;
                //}

                string PgMsg = RDS.ParseDoubleVerificationReq(EncryptedReq, DecryptedReq, IPAddress, "Flywidus", ReturnUrl);
                if (PgMsg.Contains("~"))
                {
                    if (PgMsg.Split('~').Length > 1 && PgMsg.Split('~')[0] == "yes")
                    {
                        //' Response.Redirect("../PaymentGateway.aspx?OBTID=" & ViewState("trackid") & "&IBTID=" & ViewState("IBTrackId") & "&FT=" & ViewState("FT") & "", False)
                        if (!string.IsNullOrEmpty(PgMsg.Split('~')[1]))
                        {
                            Page.Controls.Add(new LiteralControl(PgMsg.Split('~')[1]));
                        }
                        //else
                        //{
                        //    Page.Controls.Add(new LiteralControl(PgMsg.Split('~')[1]));
                        //}
                    }
                     else
                        {
                            int flag = RDS.InsertExceptionLog("RDS-IRCTC", "Verification.aspx.cs", "Check Response", "RDSVERIFICATION1", "PgMsg-" + PgMsg + "~~" + EncryptedReq, "IRCTC_ReturnUrl-" + ReturnUrl + "~~" + DecryptedReq);
                        }                    
                }
                else
                {
                        int flag = RDS.InsertExceptionLog("RDS-IRCTC", "Verification.aspx.cs", "Check Response", "RDSVERIFICATION1","PgMsg-"+ PgMsg+"~~"+ EncryptedReq, "IRCTC_ReturnUrl-" + ReturnUrl + "~~" + DecryptedReq);
                        // ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('please try after some time because payment gateway process busy - 003');", true);
                    }


            }
            }
            catch(Exception ex)
            {
                int flag = RDS.InsertExceptionLog("RDS-IRCTC", "Verification.aspx.cs", "Check Response", "RDS", ex.Message, ex.StackTrace.ToString());
            }
        }

    }
}