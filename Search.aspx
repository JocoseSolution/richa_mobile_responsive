﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Search.aspx.vb" Inherits="Search"
    MasterPageFile="~/MasterForHome.master" %>

<%--<%@ Register Src="~/UserControl/DashBoard.ascx" TagPrefix="uc1" TagName="DashBoard" %>--%>
<%@ Register Src="~/UserControl/FltSearch.ascx" TagName="IBESearch" TagPrefix="Search" %>
<%@ Register Src="~/UserControl/HotelSearch.ascx" TagPrefix="Search" TagName="HotelSearch" %>
<%@ Register Src="~/BS/UserControl/BusSearch.ascx" TagName="BusSearch" TagPrefix="Searchsss" %>
<%@ Register Src="~/UserControl/FltSearchFixDep.ascx" TagName="IBESearchDep" TagPrefix="SearchDep" %>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" ID="Cont1" runat="server">
    <script src="<%=ResolveUrl("~/Scripts/ReissueRefund.js")%>" type="text/javascript"></script>
    <%--<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>--%>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.0/jquery.min.js"></script>
    <link href="Custom_Design/css/search.css" rel="stylesheet" />


    <script type="text/javascript">
        /* Vanilla JS */

        var rightJS = {
            init: function () {
                rightJS.Tags = document.querySelectorAll('.rightJS');
                for (var i = 0; i < rightJS.Tags.length; i++) {
                    rightJS.Tags[i].style.overflow = 'hidden';
                }
                rightJS.Tags = document.querySelectorAll('.rightJS div');
                for (var i = 0; i < rightJS.Tags.length; i++) {
                    rightJS.Tags[i].style.position = 'relative';
                    rightJS.Tags[i].style.right = '-' + rightJS.Tags[i].parentElement.offsetWidth + 'px';
                }
                rightJS.loop();
            },
            loop: function () {
                for (var i = 0; i < rightJS.Tags.length; i++) {
                    var x = parseFloat(rightJS.Tags[i].style.right);
                    x++;
                    var W = rightJS.Tags[i].parentElement.offsetWidth;
                    var w = rightJS.Tags[i].offsetWidth;
                    if ((x / 100) * W > w) x = -W;
                    if (rightJS.Tags[i].parentElement.parentElement.querySelector(':hover') !== rightJS.Tags[i].parentElement) rightJS.Tags[i].style.right = x + 'px';
                }
                requestAnimationFrame(this.loop.bind(this));
            }
        };
        window.addEventListener('load', rightJS.init);

        /* JQUERY */

        $(function () {
            var rightJQ = {
                init: function () {
                    $('.rightJQ').css({
                        overflow: 'hidden'
                    });
                    $('.rightJQ').on('mouseover', function () {
                        $('div', this).stop();
                    });
                    $('.rightJQ').on('mouseout', function () {
                        $('div', this).animate({
                            right: '100%'
                        }, 15000, 'linear');
                    });
                    rightJQ.loop();
                },
                loop: function () {
                    $('.rightJQ div').css({
                        position: 'relative',
                        right: '-100%'
                    }).animate({
                        right: '100%'
                    }, 15000, 'linear', rightJQ.loop);
                }
            };
            rightJQ.init();
        });

    </script>


    <style type="text/css">
        .mobiApp {
    background: -moz-linear-gradient(left, #334755, #334755);
    background: -webkit-linear-gradient(left, #334755, #334755);
    background: -ms-linear-gradient(left, #334755, #334755);
    background: -o-linear-gradient(left, #334755, #334755);
    height: auto;
    border-top: 2px solid #07A77B;
    color: #d4dee5;
    margin-top: 110px;
}

        .u_floatL {
    float: left;
}

        .u_floatL {
    float: left;
}
        .mobiApp .head {
    margin: 0;
    padding: 0;
    margin-top: 50px;
    font-size: 28px;
    font-weight: 300;
    background:none !important;
}
        .u_fontW700 {
    font-weight: 700 !important;
}
.u_clViaWhite {
    color: #fff !important;
}

.u_clear {
    clear: both;
}

.mobiApp .u_inlineblk {
    vertical-align: top;
}
.u_inlineblk {
    display: inline-block;
}

.mobiApp .appDisc {
 font-size: 20px;
    padding: 0px 20px 0 0;
    font-weight: 300;
    margin-top: -173px;
    color: #fff;
    margin-left: 154px;
}

.mobiApp h5 {
    margin-top: 10px;
    font-size: 12px;
    font-weight: 300;
    margin-bottom: 10px;
}

.mobiApp .appDisc p {
    margin: 5px 0px;
    font-size:12px !important;
}

.INR {
    font-family: 'icomoon';
    speak: none;
    font-style: normal;
    font-weight: normal;
    font-variant: normal;
    text-transform: none;
    line-height: 1;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    color: inherit;
    font-weight: inherit;
}

.u_fontW600 {
    font-weight: 600 !important;
}
.u_clViaWhite {
    color: #fff !important;
}

.mobiApp .appVia {
    padding: 0px 0 0 20px;
    margin-top: 20px;
}

.mobiApp h5 {
    margin-top: 10px;
    font-size: 16px;
    font-weight: 300;
    margin-bottom: 10px;
}

.mobiApp .coupCode {
    padding: 10px;
    text-align: center;
    background: #12B58A;
}

.mobiApp .appVia h4 {
    font-weight: 700;
    font-size: 20px;
    margin: 0px;
}
.u_clViaWhite {
    color: #fff !important;
}

.mobiApp .appStore {
    padding-top: 40px;
}


    </style>

    <style type="text/css">
        .modal-body h1 {
            font-weight: 900;
            font-size: 2.3em;
            text-transform: uppercase;
        }

        .modal-body a.pre-order-btn {
            color: #000;
            background-color: gold;
            border-radius: 1em;
            padding: 1em;
            display: block;
            margin: 2em auto;
            width: 50%;
            font-size: 1.25em;
            font-weight: 6600;
        }

            .modal-body a.pre-order-btn:hover {
                background-color: #000;
                text-decoration: none;
                color: gold;
            }
    </style>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#myModal').modal('show');
        });
    </script>



    <!--New Design-->
    <div class="container top">
        <div class="row">
            <div class="col-sm-2 col-xs-12 desk-tab">
                <ul class="nav nav-tabs tabs-left" role="tablist">
                    <li role="presentation" class="active"><a class="k_nal mob-tab" href="#flight" aria-controls="home" role="tab" data-toggle="tab" style="width: 194px;"><span class="fa fa-plane"></span>&nbsp;  <span class="labl">Flight</span></a></li>
                    <li role="presentation"><a class="k_nal mob-tab" href="#hotel" aria-controls="profile" role="tab" data-toggle="tab" style="width: 194px;"><span class="fa fa-bed" style="color: coral;"></span>&nbsp;  <span class="labl">Hotel</span></a></li>
                    <li role="presentation"><a class="k_nal mob-tab" href="#train" aria-controls="messages" role="tab" data-toggle="tab" style="width: 194px;"><span class="fa fa-bus" style="color: cadetblue;"></span>&nbsp;  <span class="labl">Bus</span></a></li>
                    <%--                    <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab" style="border-bottom-color: #ddd; width: 260px; z-index: 1;"><span class="fa fa-dashboard"></span>&nbsp;  Dashboard</a></li>--%>

                    <li role="presentation" class="cour-menu">
                        <a href="Dash.aspx" class="mm-arr k_nal mob-tab" style="width: 194px; z-index: 1;"><span class="fa fa-dashboard" style="color: darkmagenta;"></span>&nbsp;  <span class="labl">Dashboard</span></a>

                    </li>

                </ul>
            </div>

            <div class="nav mob-tabs">
               <%-- <a class="k_nal mob-tab" href="#flight" aria-controls="home" role="tab" data-toggle="tab" style="width: 194px;"><span class="fa fa-plane"></span>&nbsp;  <span class="labl">Flight</span></a>
            <a class="k_nal mob-tab" href="#hotel" aria-controls="profile" role="tab" data-toggle="tab" style="width: 194px;"><span class="fa fa-bed" style="color: coral;"></span>&nbsp;  <span class="labl">Hotel</span></a>
                <a class="k_nal mob-tab" href="#train" aria-controls="messages" role="tab" data-toggle="tab" style="width: 194px;"><span class="fa fa-bus" style="color: cadetblue;"></span>&nbsp;  <span class="labl">Bus</span></a>--%>


                <div class="outer">
            <div class="inner actv_nw active">
                <a href="#flight" aria-controls="home" role="tab" data-toggle="tab">
                    <i class="fli_n_icn"></i>
                    <span class="icn_ttl fw700 tab-mob">flights</span>
                </a>
            </div>
            <div class="inner">
                <a href="#hotel" aria-controls="profile" role="tab" data-toggle="tab">
                    <i class="htl_n_icn"></i>
                    <span class="icn_ttl tab-mob">Hotels</span>
                </a>
            </div>
            <div class="inner">
                <a href="#train" aria-controls="messages" role="tab" data-toggle="tab">
                    <i class="bus_n_icn"></i>
                    <span class="icn_ttl tab-mob">Bus</span>
                </a>
            </div>
            
            
            <div class="inner">
                <a href="Dash.aspx">
                    <i class="das_n_icn"></i>
                    <span class="icn_ttl">Dashboard</span>
                </a>
            </div>	
            			
		
        </div>


            </div>

            <div class="col-sm-7 content">
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="flight">

                        <div>
                            <marquee class="li" direction="”right”" onmouseover="stop()" onmouseout="start()">★ Book Flight Ticket and enjoy your holidays with distinctive experience ★</marquee>
                        </div>

                        <Search:IBESearch ID="IBESearch1" runat="server" />

                    </div>




                    <div role="tabpanel" class="tab-pane" id="hotel" style="height: 395px;">

                        <Search:HotelSearch runat="server" ID="HotelSearch2" />

                    </div>



                    <div role="tabpanel" class="tab-pane" id="train" style="height: 395px;">

                        <Searchsss:BusSearch ID="Bus2" runat="server" />


                    </div>



                    <div role="tabpanel" class="tab-pane" id="settings">Dashboard</div>




                    <div class="secndblak" style="display: none">
                        <div class="container">
                            <div class="col-xs-12 col-sm-12 tab-out bgclo">
                                <!-- Tab panes -->
                                <div class="tab-content tabsbg">
                                    <div class="tab-pane active" id="Div1">
                                        <Search:IBESearch ID="IBESearch2" runat="server" />
                                    </div>
                                    <div class="tab-pane" id="Div2">
                                        <Search:HotelSearch runat="server" ID="HotelSearch1" />
                                    </div>
                                    <div class="tab-pane" id="Div3">
                                        <Searchsss:BusSearch runat="server" ID="BusSearch1" />
                                    </div>

                                    <div class="tab-pane" id="Div4">
                                        <SearchDep:IBESearchDep ID="IBESearchDep1" runat="server" />
                                    </div>


                                </div>
                                <!-- Tab panes -->
                            </div>
                        </div>
                    </div>

                </div>
            </div>





            <div class="col-md-3 tour_r" style="margin-top: 40px;display:none;">

                <!--====== TRIP INFORMATION ==========-->
                <%--<div class="tour_right tour_incl tour-ri-com">--%>
                <%--                 <h3><i class="fa fa-rss" style="color: coral;"></i>  News Feeds</h3>
                   <marquee height="100" width="100%" behavior="scroll" direction="up" scrollamount="2" onmouseover="this.stop();" onmouseout="this.start();">
  <ul>
    <li><a href="#">Hello I am Testing 1</a></li>
    <li><a href="#">Hello I am Testing 2</a></li>
    <li><a href="#">Hello I am Testing 3</a></li>
    <li><a href="#">Hello I am Testing 4</a></li>
</marquee>--%>
                <%--<div class="col-md-3">--%>
                <div class="to-ho-hotel-con">
                    <div class="to-ho-hotel-con-1">
                        <div class="hot-page2-hli-3">
                            <img src="Custom_Design/images/hci1.png" alt="">
                        </div>
                        <div class="hom-hot-av-tic">Book Now</div>
                        <img src="Images/gallery/WhatsApp%20Image%202020-07-23%20at%201.18.00%20PM.jpeg" alt="" />
                    </div>
                    <div class="to-ho-hotel-con-23">
                        <div class="to-ho-hotel-con-2"><a href="hotel-details.html">
                            <h4>Find Cheapest Flight</h4>
                        </a></div>
                        <div class="to-ho-hotel-con-3">
                            <ul>
                                <li style="float: none;">GET SET TO FLY AGAIN!!!
											<div class="dir-rat-star ho-hot-rat-star">Find Best Flight Deals Here</div>
                                </li>
                                <%--<li><span class="ho-hot-pri">Book Now</span> </li>--%>
                            </ul>
                        </div>
                    </div>
                </div>
                <%--</div>--%>
            </div>


        </div>
    </div>
    <!--New Design-->
    <br />



    <nav style="display:none;">
	<ul>

 <%--       <li><button type="button" class="btn btn-secondary" data-container="body" data-toggle="popover" data-placement="top" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
  Popover on top
</button></li>--%>

     <%--   <li>
            <a tabindex="0"
   href="#" 
   role="button" 
   data-html="true" 
   data-toggle="popover" 
   data-placement="bottom" 
   data-trigger="focus" 
   title="<b>Example popover</b> - title" 
   data-content="<div><a href='/link'><b>Geoff</b> - content</a></div><div><b>Hilary</b> - content</div>">RSS <i class="fa fa-rss" aria-hidden="true"></i></a>
        </li>--%>

		<li><a href="#" data-container="body" data-toggle="popover" data-placement="top" title="Last Booking" data-content="DEL → BOM | 17 Mar, 2020 | SG-456"><i class="fa fa-plane" aria-hidden="true"></i>  Last Searching</a></li>
        <li><a href="Report/TicketReport.aspx" > <i class="fa fa-book" ></i>  Last Booking</a></li>
		<li><a href="Report/Agent/TravelCalender.aspx" style="width: 160px;"><i class="fa fa-calendar" aria-hidden="true"></i>  Travel Calendar</a></li>
		<li><a href="Report/Accounts/BankDetails.aspx"><i class="fa fa-bank" aria-hidden="true"></i>  Bank Details</a></li>
		<li><a href="Report/Accounts/LedgerSingleOrderID.aspx"><i class="fa fa-file" aria-hidden="true"></i>  Ledger Details</a></li>
		<li><a href="Report/Accounts/AgentPanel.aspx"><i class="fa fa-inr" aria-hidden="true"></i>  Upload Amount</a></li>
		<asp:HiddenField ID="hdlastsearch" runat="server" />
	</ul>
</nav>

    <br />



    <section class="mobiApp">
 	<div class="container">
         <div class="row">
 		<div class="col-md-6">
 			<div class="u_floatL col-md-4">
 				<img src="Images/Abstract/app2.png" style="width: 111px;"/>
 			</div>
             </div>
 			<div class="u_floatL col-md-6">
 				
 				<div class="row">
 					<div class="u_inlineblk">
 						<div class="appDisc">
 							<h5>Download RichaTravel app now &amp;</h5>
 							<p>
 								    Experience <i class="INR u_fontW100"></i><span class="u_clViaWhite u_fontW600"> Richa Travels </span> in your android device<br>

 							</p>

                             <h5 class="u_alignC">Download APK File</h5>
	 						
	 							<a href="../Custom_Design/RichaTravels.apk" class="u_clViaWhite">Get File!</a>
 						</div>
 					</div>
 					
 				</div>

 				
 			</div>
 			<div class="u_clear"></div>
 		</div>
 	</div>
 </section>



    <div id="carousel-example-generic searchbg " class="carousel slide" data-ride="carousel" style="display: none;">

        <div class="clearfix"></div>

        <div class="carousel slide" id="carousel-example-captions" data-ride="carousel">

            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <img runat="server" src="~/images/img-1.jpg" alt="1" style="width: 100%;" />
                    <div class="carousel-caption">
                        <div class="toptxt">ACCOMMODATION NOW OPEN!</div>
                        <span class="toptxt">From INR 2999</span>
                    </div>
                </div>
                <div class="item">
                    <img runat="server" src="~/images/img-2.jpg" alt="1" style="width: 100%;" />
                    <div class="carousel-caption">
                        <div class="toptxt">BOOK NOW THE BEST DEALS</div>
                        <span class="toptxt">Book securely and with confidence</span>
                    </div>
                </div>
                <div class="item">
                    <img runat="server" src="~/images/img-3.jpg" alt="1" style="width: 100%;" />
                    <div class="carousel-caption">
                        <div class="toptxt">ACCOMMODATION NOW OPEN!</div>
                        <span class="toptxt">From INR 2999</span>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="searchengine searchbg " id="multisit">
                <div class="container srcmobile ">
                    <div class="row" style="margin-bottom: 0px;">
                        <div style="clear: both;"></div>
                        <div class="col-xs-12 col-sm-12" style="padding-right: 0px;">
                            <ul class="nav nav-tabs tabstab">
                                <li class="active "><a href="#home" data-toggle="tab">
                                    <span class="sprte iconcmn icnhnflight"></span>
                                    Flight
                                </a></li>
                                <li class=" "><a href="#profile" data-toggle="tab">
                                    <span class="sprte iconcmn icnhtl" aria-hidden="true"></span>
                                    Hotel
                                </a></li>
                                <li class=" "><a href="#bus" data-toggle="tab">
                                    <span class="sprte iconcmn icnhnbus" aria-hidden="true"></span>
                                    Bus
                                </a></li>

                                <li class=" "><a href="https://www.irctc.co.in/nget/train-search" target="_blank">
                                    <span class="sprte iconcmn icnhnhlydy" aria-hidden="true"></span>
                                    Train
                                </a></li>
                                <li class=" "><a href="#dep" data-toggle="tab">
                                    <span class="sprte iconcmn icnhnflight" aria-hidden="true"></span>
                                    Fixed Departure
                                </a></li>

                            </ul>
                        </div>
                    </div>
                </div>
                <div class="secndblak">
                    <div class="container">
                        <div class="col-xs-12 col-sm-12 tab-out bgclo">
                            <!-- Tab panes -->
                            <div class="tab-content tabsbg">
                                <div class="tab-pane active" id="home">
                                    <Search:IBESearch ID="IBE_CP" runat="server" />
                                </div>
                                <div class="tab-pane" id="profile">
                                    <Search:HotelSearch runat="server" ID="HotelSearch" />
                                </div>
                                <div class="tab-pane" id="bus">
                                    <Searchsss:BusSearch runat="server" ID="BusSearch" />
                                </div>

                                <div class="tab-pane" id="dep">
                                    <SearchDep:IBESearchDep ID="FixDep" runat="server" />
                                </div>


                            </div>
                            <!-- Tab panes -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>





    <div class="booking-remarks" id="divStockistList" style="display: none;">
        <img id="imgCloseCHSch" src="Images/closebox.png" alt="" style="float: right; z-index: 9999; cursor: pointer; margin-top: -27px; margin-right: -20px;"
            title="Close" />

    </div>
    <div class="row" style="display: none;">
        <div id="toPopup" class="tbltbl large-12 medium-12 small-12">
            <div class="close">
            </div>
            <span class="ecs_tooltip">Press Esc to close <span class="arrow"></span></span>
            <div id="popup_content">
                <!--your content start-->
                <table border="0" cellpadding="10" cellspacing="5" style="font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: normal; font-style: normal; color: #000000">
                    <tr>
                        <td>
                            <b>PNR :</b> <span id="PNR"></span>
                            <input id="txtPNRNO" name="txtPNRNO" type="hidden" />
                        </td>
                        <td id="TktNoInfo" style="display: none;">
                            <b>Ticket No:</b> <span id="TktNo"></span>
                        </td>
                    </tr>
                    <tr>
                        <td style="display: none;" id="PaxnameInfoResu">
                            <b>PAX NAME :</b> <span id="Paxname"></span>
                        </td>
                        <td style="display: none;" id="PaxnameInfoRefnd">
                            <div id="Refunddtldata" class="large-12 medium-12 small-12"></div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <b>
                                <span id="RemarksTypetext"></span>Remark 
                            </b>
                            <input id="RemarksType" name="RemarksType" type="hidden" />

                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <textarea id="txtRemark" name="txtRemark" cols="56" rows="1" style="border: thin solid #808080"></textarea>
                        </td>
                    </tr>
                    <tr id="trCancelledBy" visible="false">
                        <td>
                            <b>Cancelled By:</b>
                        </td>
                        <td>
                            <asp:DropDownList ID="DrpCancelledBy" CssClass="drop" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div style="width: 20%; margin-left: 40%;">
                                <asp:Button ID="btnRemark" runat="server" Text="Submit" CssClass="buttonfltbk rgt w20" />
                                <input id="txtPaxid" name="txtPaxid" type="hidden" />
                                <input id="txtPaxType" name="txtPaxType" type="hidden" />
                                <input id="txtSectorid" name="txtSectorid" type="hidden" />
                                <input id="txtOrderid" name="txtOrderid" type="hidden" />
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            <!--your content end-->
        </div>

        <div class="loader">
        </div>
        <div id="backgroundPopup">
        </div>
        <div id="HourDeparturePopup">
            <div class="close11">
            </div>
            <span class="ecs_tooltip">Press Esc to close <span class="arrocol-md-4 col-sm-6 col-xs-5 nopadw"></span></span>
            <div class="HourDeparturepopup_content">

                <div class="large-12 medium-12 small-12">
                    <div class="clear"></div>
                    <div class="large-12 medium-12 small-12 text-center ">Click “OK” to proceed for offline request.</div>
                    <div class="clear"></div>
                    <div class="large-4 medium-4 small-4"></div>
                    <div style="margin-left: 100px;" class="rgt w100 buttonfltbkss">OK</div>
                    <input id="txtPaxid_4HourDeparture" name="txtPaxid_4HourDeparture" type="hidden" />
                </div>
            </div>
        </div>


        <div id="htlRfndPopup">
            <div class="refundbox">

                <div style="font-weight: bold; font-size: 16px; text-align: center; width: 100%;">

                    <div id="RemarkTitle"></div>
                    <div style="float: right; width: 20px; height: 20px; margin: -20px -13px 0 0;">
                        <a href="javascript:ShowHide('hide');">
                            <img src="<%=ResolveUrl("~/Images/close.png") %>" height="20px" /></a>
                    </div>

                </div>

                <div class="large-12 medium-12 small-12">

                    <div class="laege-1 medium-1 small-1 columns bld">Hotel Name:</div>
                    <div class="laege-5 medium-5 small-5 columns" id="HotelName"></div>

                    <div class="clear"></div>
                    <div class="clear"></div>
                    <div class="laege-1 medium-1 small-1 columns bld">
                        Net Cost:
                    </div>
                    <div class="laege-1 medium-1 small-1 columns" id="amt"></div>
                    <div class="large-1 medium-1 small-1  medium-push-1 columns bld">
                        No. of Room:
                    </div>
                    <div class="laege-1 medium-1 small-1 large-push-1 medium-push-1 columns" id="room"></div>

                    <div class="laege-1 medium-1 small-1 large-push-1 medium-push-1 columns bld">No. of Night:</div>
                    <div class="laege-1 medium-1 small-1 large-push-1 medium-push-1 columns" id="night"></div>
                    <div class="laege-1 medium-1 small-1 columns bld">No. of Adult:</div>
                    <div class="laege-1 medium-1 small-1 columns" id="adt"></div>
                    <div class="laege-1 medium-1 small-1 large-push-1 medium-push-1 columns bld">No. of Child:</div>
                    <div class="laege-1 medium-1 small-1 large-push-1 medium-push-1 columns end" id="chd"></div>
                    <div class="clear"></div>
                </div>
                <div class="clear"></div>
                <div id="policy" class="large-12 medium-12 small-12"></div>

                <div class="clear"></div>
                <div class="large-12 medium-12 small-12">

                    <div style="font-weight: bold;">
                        Cancellation Remark:
                    </div>
                    <div>
                        <textarea id="txtRemarkss" cols="40" rows="2" name="txtRemarkss"></textarea>
                    </div>
                    <div style="float: right; padding-left: 40px;">
                        <asp:Button ID="btn_Refund" runat="server" Text="Hotel Cancelltion" CssClass="button" ToolTip="Auto Cancel of Hotel"
                            OnClientClick="return RemarkValidation('cancellation')" />
                    </div>
                    <div class="clear1"></div>
                </div>

                <div>
                    <input id="StartDate" type="hidden" name="StartDate" />
                    <input id="EndDate" type="hidden" name="EndDate" />
                    <input id="Parcial" type="hidden" name="Parcial" value="false" />
                    <input id="OrderIDS" type="hidden" name="OrderIDS" />
                </div>
                <div style="visibility: hidden;">

                    <div style="font-weight: bold;">
                        Full Cancellation
                                                        <input id="ChkFullCan" type="radio" name="Can" checked="checked" />
                    </div>
                    <div style="font-weight: bold; padding-left: 20px;">
                        Partial Cancellation
                                                        <input id="ChkParcialCan" type="radio" name="Can" />
                    </div>

                    <%-- <tr><td colspan="3" class="PrcialRegardCancelMsg"></td></tr>--%>
                </div>
            </div>
        </div>
    </div>




    <%-- <link href="<%=ResolveUrl("~/CSS/PopupStyle.css")%>" rel="stylesheet" type="text/css" />
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" rel="stylesheet" />
    <link href="<%= ResolveUrl("~/Styles/jAlertCss.css")%>" rel="stylesheet" />

    <script src="<%=ResolveUrl("~/Hotel/JS/HotelRefund.js")%>" type="text/javascript"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/PopupScript.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/alert.js")%>"></script>--%>
    <%--    <script type="text/javascript">
        $("#rdbMultiCity").click(function () {

            $("#multisit").removeClass("searchengine searchbg").addClass("searchenginess searchbg ");
            $(".toptxt").hide();

        });

        $(".toptxt").show();

        //$('.carousel').carousel({
        //    interval: 1000
        //})

        if ('<%=Request("Htl")%>' == 'H') {
            $("#img2-").show();
            $("#img1").hide();
        }
        else {
            $("#img1").show();
            $("#img2").hide();
        }


    </script>--%>
    <%--<script type="text/javascript">

        $(document).ready(function () {


            $(".onewayss").removeClass("col-md-4 nopad text-search mltcs").addClass("col-md-5 nopad text-search mltcs");

            $("#rdbOneWay").click(function () {

                $(".onewayss").removeClass("col-md-4 nopad text-search mltcs").addClass("col-md-5 nopad text-search mltcs");
            });

            $("#rdbRoundTrip").click(function () {

                $(".onewayss").removeClass("col-md-5 nopad text-search mltcs").addClass("col-md-4 nopad text-search mltcs");
            });
            $("#rdbMultiCity").click(function () {
                $(".onewayss").removeClass("col-md-4 nopad text-search mltcs").addClass("col-md-5 nopad text-search mltcs");
            });




            $("#CB_GroupSearch").click(function () {
                if ($(this).is(":checked")) {
                    // $("#box").hide();
                    $(".Traveller").hide();
                    $("#rdbRoundTrip").attr("checked", true);
                    $("#rdbOneWay").attr("checked", false);

                } else {
                    // $("#box").show();
                    $(".Traveller").show();
                }
            });
        });
    </script>--%>

    <script type="text/javascript">
        $(document).ready(function () {
            $('.dropdown-submenu a.test').on("click", function (e) {
                $(this).next('ul').toggle();
                e.stopPropagation();
                e.preventDefault();
            });
        });
    </script>

    <script type="text/javascript">

        $('.btn').on('click', function () {
            var $this = $(this);
            $this.button('loading');
            setTimeout(function () {
                $this.button('reset');
            }, 8000);
        });
    </script>

    <script type="text/javascript">
        $(function () {
            debugger;
            // Enables popover
            $("[data-toggle=popover]").popover();
            container: 'body'
            $("lsearch").prepend($("hdlastsearch").val);
        });
    </script>



    <%-- <script type="text/javascript" src="<%=ResolveUrl("js/jquery-latest.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("js/bootstrap.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("js/wow.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("js/materialize.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("js/custom.js")%>"></script>--%>
</asp:Content>
